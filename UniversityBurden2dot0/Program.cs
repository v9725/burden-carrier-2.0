﻿using System;
using System.Windows.Forms;

namespace UniversityBurden2dot0
{
    static class Program
    {
        /// <summary>
        /// Главный контроллера приложения.
        /// </summary>
        private static Controler mainControler;

        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Системные вызовы
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //Создание экземпляра контроллера и формы
            mainControler = new Controler();
            MainForm mainForm = new MainForm(mainControler);

            //Проверка ДБ
            if (mainControler.InitCheck())
            {
                //Проверка наличия словаря Псевдонимов.
                if (!System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + "AliasesDictionary.xml"))
                {
                    //Инициализация Псевдонимов.
                    Utils.AliasDictionary.InitStorage();
                    
                    //Вызов формы
                    Application.Run(mainForm);
                }
                else
                {
                    //Загрузка Псевдонимов.
                    if (Utils.AliasDictionary.LoadAliasesFromStorage())
                    {
                        //Вызов формы
                        Application.Run(mainForm);
                    }
                    else
                    {
                        MessageBox.Show("Something went wrong while loading Aliases!");
                        //логирование
                        Application.Exit();
                    }
                }
            }
            else
            {
                Application.Exit();
            }
        }
    }
}

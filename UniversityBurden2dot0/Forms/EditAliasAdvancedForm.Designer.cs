﻿namespace UniversityBurden2dot0.Forms
{
    partial class EditAliasAdvancedForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AliasesNamesComboBox = new System.Windows.Forms.ComboBox();
            this.AliasesDataGridView = new System.Windows.Forms.DataGridView();
            this.AliasName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddDeleteColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.SaveColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.AddAliasNameButton = new System.Windows.Forms.Button();
            this.EditAliasNameButton = new System.Windows.Forms.Button();
            this.DeleteAliasNameButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.AliasesDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // AliasesNamesComboBox
            // 
            this.AliasesNamesComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AliasesNamesComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AliasesNamesComboBox.FormattingEnabled = true;
            this.AliasesNamesComboBox.Location = new System.Drawing.Point(13, 13);
            this.AliasesNamesComboBox.Name = "AliasesNamesComboBox";
            this.AliasesNamesComboBox.Size = new System.Drawing.Size(334, 26);
            this.AliasesNamesComboBox.TabIndex = 0;
            // 
            // AliasesDataGridView
            // 
            this.AliasesDataGridView.AllowUserToDeleteRows = false;
            this.AliasesDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AliasesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.AliasesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AliasName,
            this.AddDeleteColumn,
            this.SaveColumn});
            this.AliasesDataGridView.Location = new System.Drawing.Point(12, 45);
            this.AliasesDataGridView.Name = "AliasesDataGridView";
            this.AliasesDataGridView.ReadOnly = true;
            this.AliasesDataGridView.RowTemplate.Height = 24;
            this.AliasesDataGridView.Size = new System.Drawing.Size(434, 245);
            this.AliasesDataGridView.TabIndex = 1;
            // 
            // AliasName
            // 
            this.AliasName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AliasName.HeaderText = "Название";
            this.AliasName.Name = "AliasName";
            this.AliasName.ReadOnly = true;
            // 
            // AddDeleteColumn
            // 
            this.AddDeleteColumn.HeaderText = " ";
            this.AddDeleteColumn.Name = "AddDeleteColumn";
            this.AddDeleteColumn.ReadOnly = true;
            this.AddDeleteColumn.Width = 25;
            // 
            // SaveColumn
            // 
            this.SaveColumn.HeaderText = "  ";
            this.SaveColumn.Name = "SaveColumn";
            this.SaveColumn.ReadOnly = true;
            this.SaveColumn.Width = 25;
            // 
            // AddAliasNameButton
            // 
            this.AddAliasNameButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AddAliasNameButton.Location = new System.Drawing.Point(353, 12);
            this.AddAliasNameButton.Name = "AddAliasNameButton";
            this.AddAliasNameButton.Size = new System.Drawing.Size(27, 27);
            this.AddAliasNameButton.TabIndex = 2;
            this.AddAliasNameButton.Text = "+";
            this.AddAliasNameButton.UseVisualStyleBackColor = true;
            // 
            // EditAliasNameButton
            // 
            this.EditAliasNameButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.EditAliasNameButton.Location = new System.Drawing.Point(386, 12);
            this.EditAliasNameButton.Name = "EditAliasNameButton";
            this.EditAliasNameButton.Size = new System.Drawing.Size(27, 27);
            this.EditAliasNameButton.TabIndex = 3;
            this.EditAliasNameButton.Text = "/";
            this.EditAliasNameButton.UseVisualStyleBackColor = true;
            // 
            // DeleteAliasNameButton
            // 
            this.DeleteAliasNameButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DeleteAliasNameButton.Location = new System.Drawing.Point(419, 12);
            this.DeleteAliasNameButton.Name = "DeleteAliasNameButton";
            this.DeleteAliasNameButton.Size = new System.Drawing.Size(27, 27);
            this.DeleteAliasNameButton.TabIndex = 4;
            this.DeleteAliasNameButton.Text = "X";
            this.DeleteAliasNameButton.UseVisualStyleBackColor = true;
            // 
            // EditAliasForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 303);
            this.Controls.Add(this.DeleteAliasNameButton);
            this.Controls.Add(this.EditAliasNameButton);
            this.Controls.Add(this.AddAliasNameButton);
            this.Controls.Add(this.AliasesDataGridView);
            this.Controls.Add(this.AliasesNamesComboBox);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EditAliasForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Alias Editing";
            ((System.ComponentModel.ISupportInitialize)(this.AliasesDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox AliasesNamesComboBox;
        private System.Windows.Forms.DataGridView AliasesDataGridView;
        private System.Windows.Forms.Button AddAliasNameButton;
        private System.Windows.Forms.Button EditAliasNameButton;
        private System.Windows.Forms.Button DeleteAliasNameButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn AliasName;
        private System.Windows.Forms.DataGridViewButtonColumn AddDeleteColumn;
        private System.Windows.Forms.DataGridViewButtonColumn SaveColumn;
    }
}
﻿namespace UniversityBurden2dot0
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddLoadButton = new System.Windows.Forms.Button();
            this.ClearContentButton = new System.Windows.Forms.Button();
            this.SettingsButton = new System.Windows.Forms.Button();
            this.UpdateFilesButton = new System.Windows.Forms.Button();
            this.HideEmptyLoadEntriesCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowRowsNumberCheckBox = new System.Windows.Forms.CheckBox();
            this.AboutButton = new System.Windows.Forms.Button();
            this.WorkSheetDataGrid = new System.Windows.Forms.DataGridView();
            this.DBEntriesListBox = new System.Windows.Forms.ListBox();
            this.StatsDataGridView = new System.Windows.Forms.DataGridView();
            this.ViewSettingsGroupBox = new System.Windows.Forms.GroupBox();
            this.GridViewsSplitContainer = new System.Windows.Forms.SplitContainer();
            this.SecondHalfButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.WorkSheetDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatsDataGridView)).BeginInit();
            this.ViewSettingsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewsSplitContainer)).BeginInit();
            this.GridViewsSplitContainer.Panel1.SuspendLayout();
            this.GridViewsSplitContainer.Panel2.SuspendLayout();
            this.GridViewsSplitContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // AddLoadButton
            // 
            this.AddLoadButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AddLoadButton.Location = new System.Drawing.Point(12, 7);
            this.AddLoadButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.AddLoadButton.Name = "AddLoadButton";
            this.AddLoadButton.Size = new System.Drawing.Size(34, 34);
            this.AddLoadButton.TabIndex = 0;
            this.AddLoadButton.UseVisualStyleBackColor = true;
            this.AddLoadButton.Click += new System.EventHandler(this.AddLoadButton_Click);
            // 
            // ClearContentButton
            // 
            this.ClearContentButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClearContentButton.Location = new System.Drawing.Point(92, 7);
            this.ClearContentButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ClearContentButton.Name = "ClearContentButton";
            this.ClearContentButton.Size = new System.Drawing.Size(34, 34);
            this.ClearContentButton.TabIndex = 3;
            this.ClearContentButton.UseVisualStyleBackColor = true;
            this.ClearContentButton.Click += new System.EventHandler(this.ClearContentButton_Click);
            // 
            // SettingsButton
            // 
            this.SettingsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SettingsButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SettingsButton.Location = new System.Drawing.Point(1251, 7);
            this.SettingsButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SettingsButton.Name = "SettingsButton";
            this.SettingsButton.Size = new System.Drawing.Size(34, 34);
            this.SettingsButton.TabIndex = 8;
            this.SettingsButton.UseVisualStyleBackColor = true;
            this.SettingsButton.Click += new System.EventHandler(this.SettingsButton_Click);
            // 
            // UpdateFilesButton
            // 
            this.UpdateFilesButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.UpdateFilesButton.Location = new System.Drawing.Point(52, 7);
            this.UpdateFilesButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.UpdateFilesButton.Name = "UpdateFilesButton";
            this.UpdateFilesButton.Size = new System.Drawing.Size(34, 34);
            this.UpdateFilesButton.TabIndex = 11;
            this.UpdateFilesButton.UseVisualStyleBackColor = true;
            this.UpdateFilesButton.Click += new System.EventHandler(this.UpdateFilesButton_Click);
            // 
            // HideEmptyLoadEntriesCheckBox
            // 
            this.HideEmptyLoadEntriesCheckBox.AutoSize = true;
            this.HideEmptyLoadEntriesCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HideEmptyLoadEntriesCheckBox.Location = new System.Drawing.Point(0, 26);
            this.HideEmptyLoadEntriesCheckBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.HideEmptyLoadEntriesCheckBox.Name = "HideEmptyLoadEntriesCheckBox";
            this.HideEmptyLoadEntriesCheckBox.Size = new System.Drawing.Size(200, 22);
            this.HideEmptyLoadEntriesCheckBox.TabIndex = 12;
            this.HideEmptyLoadEntriesCheckBox.Text = "Сховати порожні записи";
            this.HideEmptyLoadEntriesCheckBox.UseVisualStyleBackColor = true;
            this.HideEmptyLoadEntriesCheckBox.CheckedChanged += new System.EventHandler(this.HideEmptyLoadEntriesCheckBox_CheckedChanged);
            // 
            // ShowRowsNumberCheckBox
            // 
            this.ShowRowsNumberCheckBox.AutoSize = true;
            this.ShowRowsNumberCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ShowRowsNumberCheckBox.Location = new System.Drawing.Point(0, 54);
            this.ShowRowsNumberCheckBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ShowRowsNumberCheckBox.Name = "ShowRowsNumberCheckBox";
            this.ShowRowsNumberCheckBox.Size = new System.Drawing.Size(197, 22);
            this.ShowRowsNumberCheckBox.TabIndex = 13;
            this.ShowRowsNumberCheckBox.Text = "Показати номери строк";
            this.ShowRowsNumberCheckBox.UseVisualStyleBackColor = true;
            this.ShowRowsNumberCheckBox.CheckedChanged += new System.EventHandler(this.ShowRowsNumberCheckBox_CheckedChanged);
            // 
            // AboutButton
            // 
            this.AboutButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AboutButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AboutButton.Location = new System.Drawing.Point(1291, 7);
            this.AboutButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.AboutButton.Name = "AboutButton";
            this.AboutButton.Size = new System.Drawing.Size(34, 34);
            this.AboutButton.TabIndex = 14;
            this.AboutButton.Text = "?";
            this.AboutButton.UseVisualStyleBackColor = true;
            this.AboutButton.Click += new System.EventHandler(this.AboutButton_Click);
            // 
            // WorkSheetDataGrid
            // 
            this.WorkSheetDataGrid.AllowUserToAddRows = false;
            this.WorkSheetDataGrid.AllowUserToDeleteRows = false;
            this.WorkSheetDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.WorkSheetDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WorkSheetDataGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.WorkSheetDataGrid.Location = new System.Drawing.Point(0, 0);
            this.WorkSheetDataGrid.Margin = new System.Windows.Forms.Padding(3, 1, 3, 2);
            this.WorkSheetDataGrid.Name = "WorkSheetDataGrid";
            this.WorkSheetDataGrid.ReadOnly = true;
            this.WorkSheetDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.WorkSheetDataGrid.RowTemplate.Height = 24;
            this.WorkSheetDataGrid.Size = new System.Drawing.Size(989, 618);
            this.WorkSheetDataGrid.TabIndex = 4;
            // 
            // DBEntriesListBox
            // 
            this.DBEntriesListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.DBEntriesListBox.FormattingEnabled = true;
            this.DBEntriesListBox.HorizontalScrollbar = true;
            this.DBEntriesListBox.ItemHeight = 16;
            this.DBEntriesListBox.Location = new System.Drawing.Point(12, 46);
            this.DBEntriesListBox.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.DBEntriesListBox.Name = "DBEntriesListBox";
            this.DBEntriesListBox.Size = new System.Drawing.Size(316, 596);
            this.DBEntriesListBox.TabIndex = 9;
            this.DBEntriesListBox.SelectedIndexChanged += new System.EventHandler(this.DBEntriesListBox_SelectedIndexChanged);
            // 
            // StatsDataGridView
            // 
            this.StatsDataGridView.AllowUserToAddRows = false;
            this.StatsDataGridView.AllowUserToDeleteRows = false;
            this.StatsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.StatsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StatsDataGridView.Location = new System.Drawing.Point(0, 0);
            this.StatsDataGridView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.StatsDataGridView.Name = "StatsDataGridView";
            this.StatsDataGridView.ReadOnly = true;
            this.StatsDataGridView.RowTemplate.Height = 24;
            this.StatsDataGridView.Size = new System.Drawing.Size(989, 100);
            this.StatsDataGridView.TabIndex = 15;
            // 
            // ViewSettingsGroupBox
            // 
            this.ViewSettingsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ViewSettingsGroupBox.Controls.Add(this.HideEmptyLoadEntriesCheckBox);
            this.ViewSettingsGroupBox.Controls.Add(this.ShowRowsNumberCheckBox);
            this.ViewSettingsGroupBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ViewSettingsGroupBox.Location = new System.Drawing.Point(12, 663);
            this.ViewSettingsGroupBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ViewSettingsGroupBox.Name = "ViewSettingsGroupBox";
            this.ViewSettingsGroupBox.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ViewSettingsGroupBox.Size = new System.Drawing.Size(317, 106);
            this.ViewSettingsGroupBox.TabIndex = 16;
            this.ViewSettingsGroupBox.TabStop = false;
            this.ViewSettingsGroupBox.Text = "Налаштування Відображення";
            // 
            // GridViewsSplitContainer
            // 
            this.GridViewsSplitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridViewsSplitContainer.Location = new System.Drawing.Point(336, 46);
            this.GridViewsSplitContainer.Margin = new System.Windows.Forms.Padding(4);
            this.GridViewsSplitContainer.Name = "GridViewsSplitContainer";
            this.GridViewsSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // GridViewsSplitContainer.Panel1
            // 
            this.GridViewsSplitContainer.Panel1.Controls.Add(this.WorkSheetDataGrid);
            // 
            // GridViewsSplitContainer.Panel2
            // 
            this.GridViewsSplitContainer.Panel2.Controls.Add(this.StatsDataGridView);
            this.GridViewsSplitContainer.Size = new System.Drawing.Size(989, 723);
            this.GridViewsSplitContainer.SplitterDistance = 618;
            this.GridViewsSplitContainer.SplitterWidth = 5;
            this.GridViewsSplitContainer.TabIndex = 17;
            // 
            // SecondHalfButton
            // 
            this.SecondHalfButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SecondHalfButton.Location = new System.Drawing.Point(132, 7);
            this.SecondHalfButton.Name = "SecondHalfButton";
            this.SecondHalfButton.Size = new System.Drawing.Size(34, 34);
            this.SecondHalfButton.TabIndex = 18;
            this.SecondHalfButton.UseVisualStyleBackColor = true;
            this.SecondHalfButton.Click += new System.EventHandler(this.SecondHalfButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1341, 784);
            this.Controls.Add(this.SecondHalfButton);
            this.Controls.Add(this.GridViewsSplitContainer);
            this.Controls.Add(this.ViewSettingsGroupBox);
            this.Controls.Add(this.DBEntriesListBox);
            this.Controls.Add(this.AboutButton);
            this.Controls.Add(this.UpdateFilesButton);
            this.Controls.Add(this.SettingsButton);
            this.Controls.Add(this.ClearContentButton);
            this.Controls.Add(this.AddLoadButton);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NURE WorkLoad Handler";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.WorkSheetDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatsDataGridView)).EndInit();
            this.ViewSettingsGroupBox.ResumeLayout(false);
            this.ViewSettingsGroupBox.PerformLayout();
            this.GridViewsSplitContainer.Panel1.ResumeLayout(false);
            this.GridViewsSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridViewsSplitContainer)).EndInit();
            this.GridViewsSplitContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button AddLoadButton;
        private System.Windows.Forms.Button ClearContentButton;
        private System.Windows.Forms.Button SettingsButton;
        private System.Windows.Forms.Button UpdateFilesButton;
        private System.Windows.Forms.CheckBox HideEmptyLoadEntriesCheckBox;
        private System.Windows.Forms.CheckBox ShowRowsNumberCheckBox;
        private System.Windows.Forms.Button AboutButton;
        private System.Windows.Forms.DataGridView WorkSheetDataGrid;
        private System.Windows.Forms.ListBox DBEntriesListBox;
        private System.Windows.Forms.DataGridView StatsDataGridView;
        private System.Windows.Forms.GroupBox ViewSettingsGroupBox;
        private System.Windows.Forms.SplitContainer GridViewsSplitContainer;
        private System.Windows.Forms.Button SecondHalfButton;
    }
}


﻿
namespace UniversityBurden2dot0.DataModels
{
    /// <summary>
    /// Класс, содержащий компоненты названия файла-модели.
    /// </summary>
    public class FileTitle : System.IEquatable<FileTitle>
    {
        #region Fields

        /// <summary>
        /// Идентификатор файла в БД.
        /// </summary>
        public int FileId { get; set; }

        /// <summary>
        /// Дата загрузки файла в систему.
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// Имя загружаемого файла (без расширения).
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Версия файла в системе.
        /// </summary>
        public int Version { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Конструктор класса по-умолчанию.
        /// </summary>
        /// <param name="date">Дата загрузки файла в систему.</param>
        /// <param name="filename">Имя загружаемого файла (без расширения).</param>
        /// <param name="versrion">Версия файла в системе.</param>
        /// <param name="fileid">Идентификатор файла в БД.</param>
        public FileTitle(
            string date = "",
            string filename = "",
            int version = 0,
            int fileid = -1
            )
        {
            FileId = fileid;
            Date = date;
            FileName = filename;
            Version = version;
        }

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        /// <param name="titleString">Строка с названием модели.</param>
        public FileTitle(string titleString)
        {
            string[] titleComponents = titleString.Split('|');

            titleComponents[0] = titleComponents[0].TrimEnd(' ');   //date [+version]
            titleComponents[1] = titleComponents[1].TrimStart(' '); //filename

            if (titleComponents[0].Contains("_"))
            {
                FileName = titleComponents[1];

                Date = titleComponents[0].Split('_')[0];
                Version = int.Parse(titleComponents[0].Split('_')[1]);
            }
            else
            {
                FileName = titleComponents[1];

                Date = titleComponents[0];
                Version = 0;
            }

            FileId = -1;
        }

        #endregion

        #region Methods

        public static bool operator == (FileTitle file1, FileTitle file2)
        {
            if (!ReferenceEquals(file1, file2))
            {
                if (ReferenceEquals(file1, null) || ReferenceEquals(file2, null))
                {
                    return false;
                }
                else
                {
                    return
                        file1.Date == file2.Date &&
                        file1.FileName == file2.FileName &&
                        file1.Version == file2.Version;
                }
            }
            else
            {
                return true;
            }
        }

        public static bool operator != (FileTitle file1, FileTitle file2)
        {
            return !(file1 == file2);
        }

        public bool Equals(FileTitle other)
        {
            if (!ReferenceEquals(this, other))
            {
                if (ReferenceEquals(null, other))
                {
                    return false;
                }
                else
                {
                    return
                        this.Date == other.Date &&
                        this.FileName == other.FileName &&
                        this.Version == other.Version;
                }
            }
            else
            {
                return true;
            }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            return obj.GetType() == GetType() && Equals((FileTitle)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Date.GetHashCode() + FileName.GetHashCode()) * Version.GetHashCode();
            }
        }

        public override string ToString()
        {
            return (Version > 0) ?
                string.Format("{0}_{1} | {2}", Date, Version.ToString(), FileName) :
                string.Format("{0} | {1}", Date, FileName);
        }

        #endregion
    }
}

﻿using System;
using System.Windows.Forms;

namespace UniversityBurden2dot0.Forms
{
    /// <summary>
    /// Форма визуализирующая конфигурацию приложения.
    /// </summary>
    public partial class SettingsForm : Form
    {
        #region Fields and Properties

        /// <summary>
        /// Флаг-индикатор того, что конфигурация изменила своё состояние.
        /// </summary>
        private bool _settingsChanged;

        /// <summary>
        /// Флаг-индикатор того, что изменение настроек сохранено.
        /// </summary>
        private bool _settingsSaved;

        /// <summary>
        /// Флаг-индликатор того, что несохранённые изменения настроек отбросили.
        /// </summary>
        private bool _changesDiscarded;


        /// <summary>
        /// Строка для дополнительного сохрания пути.
        /// </summary>
        public string PreviousDBPath;

        /// <summary>
        /// Флаг смены пути к ДБ.
        /// </summary>
        public bool DBPathChanged;

        #endregion

        #region Constuсtor, Loading and Closing

        /// <summary>
        /// Конструктор формы.
        /// </summary>
        public SettingsForm()
        {
            //Системная инициализация компонентов.
            InitializeComponent();

            //Подтягиваем ресурсы и служебные поля.
            this.Icon = Properties.Resources.settings_icon;
            PreviousDBPath = Properties.Settings.Default.DBFilePath;
        }

        /// <summary>
        /// Обработчик события загрузки формы.
        /// </summary>
        private void SettingsForm_Load(object sender, EventArgs e)
        {
            //Подтягиваем параметры из настроек.
            HoursManualСheckBox.Checked = Properties.Settings.Default.AlwaysAskHours;
            DBPathTextBox.Text = Properties.Settings.Default.DBFilePath;

            //Выставляем флаги.
            _settingsChanged = false;
            _settingsSaved = false;
            _changesDiscarded = false;
            DBPathChanged = false;

            this.ActiveControl = ApplySettingsButton;
        }

        /// <summary>
        /// Обработчик события закрытия формы.
        /// </summary>
        private void SettingsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_settingsChanged && !_settingsSaved && !_changesDiscarded)
            {
                if (MessageBox.Show(
                    "You have unsaved changes!\nDo you want to discard them?",
                    "Warning!",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning
                ) == DialogResult.Yes)
                {
                    //Сбрассываем изменения к сохранённым.
                    Properties.Settings.Default.Reload();

                    //Выстявляем флаги.
                    _settingsSaved = false;
                    _changesDiscarded = true;

                    //Закрываем форму.
                    this.DialogResult = DialogResult.Cancel;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        #endregion

        #region Items Changing Handlers

        /// <summary>
        /// Обработчик события смены состояния чекбокса Ручного распределения часов.
        /// </summary>
        private void HoursManualСheckBox_CheckStateChanged(object sender, EventArgs e)
        {
            if (!_settingsChanged) { _settingsChanged = true; }
        }

        /// <summary>
        /// Обработчик сотбытия смены строки пути к БД.
        /// </summary>
        private void DBPathTextBox_TextChanged(object sender, EventArgs e)
        {
            if (!_settingsChanged && DBPathTextBox.Text != PreviousDBPath) { DBPathChanged = _settingsChanged = true; }
        }

        private void AliasEditButton_Click(object sender, EventArgs e)
        {
            AliasEditorForm form = new AliasEditorForm();
            form.ShowDialog();
        }

        #endregion

        #region Dialog Buttons (Apply&Cansel)

        /// <summary>
        /// Закрывает форму с результатом "Отмена".
        /// </summary>
        private void CancelSettingsButton_Click(object sender, EventArgs e)
        {
            if (_settingsChanged && !_settingsSaved)
            {
                if (MessageBox.Show(
                    "You have unsaved changes!\nDo you want to discard them?",
                    "Warning!",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning
                ) == DialogResult.Yes)
                {
                    //Сбрассываем изменения к сохранённым.
                    Properties.Settings.Default.Reload();

                    //Выстявляем флаги.
                    _settingsSaved = false;
                    _changesDiscarded = true;

                    //Закрываем форму.
                    this.DialogResult = DialogResult.Cancel;
                    this.Close();
                }
            }
            else
            {
                //Сбрассываем изменения к сохранённым.
                Properties.Settings.Default.Reload();

                //Выстявляем флаги.
                _settingsSaved = false;
                _changesDiscarded = true;

                //Закрываем форму.
                this.DialogResult = DialogResult.Cancel;
                this.Close();
            }
        }

        /// <summary>
        /// Закрывает форму с результатом "ОК".
        /// </summary>
        private void ApplySettingsButton_Click(object sender, EventArgs e)
        {
            //Применяем настройки.
            Properties.Settings.Default.AlwaysAskHours = HoursManualСheckBox.Checked;
            if (PreviousDBPath != DBPathTextBox.Text)
            { Properties.Settings.Default.DBFilePath = DBPathTextBox.Text; }

            //Сохраняем настройки.
            Properties.Settings.Default.Save();

            //Выстявляем флаги.
            _settingsSaved = true;
            _changesDiscarded = false;

            //Закрываем форму.
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        #endregion

        #region General

        private void BrowseDBButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog
            {
                Multiselect = false,
                Filter = "database files (*.db)|*.db|All files (*.*)|*.*",
                FilterIndex = 2,
                RestoreDirectory = true
            };

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                DBPathTextBox.Text = dlg.FileName;
            }
            else
            { return; }
        }

        #endregion
    }
}

﻿using System;
using System.Data;
using System.Threading.Tasks;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using Marshal = System.Runtime.InteropServices.Marshal;

namespace UniversityBurden2dot0.Handlers
{
    /// <summary>
    /// Класс, осуществляющий взаимодействие с XLS-таблицами через скрытый экземпляр Microsoft Excel.
    /// </summary>
    public class XLSHandler
    {
        #region Fields and Properties

        #region Privates

        /// <summary>
        /// Ссылка на экземпляр приложения Excel.
        /// </summary>
        private Excel.Application _excelApp = null;
        
        /// <summary>
        /// Ссылка на рабочую книгу.
        /// </summary>
        private Excel.Workbook _xWorkBook = null;

        /// <summary>
        /// Служебный флаг состояния Excel-приложения.
        /// </summary>
        private bool _isOpened = false;

        #endregion

        #region Publics

        /// <summary>
        /// Экземпляр набора данных -- программного эквивалента рабочей книги.
        /// </summary>
        public System.Data.DataSet FileContent { get; private set; }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        public XLSHandler()
        {
            /////////////////////////////
            //CacheInit();
            /////////////////////////////
        }

        #endregion

        #region Closing Method

        /// <summary>
        /// Принудительно закрывает открытый файл и экземпляр приложения.
        /// </summary>
        public void ForceCloseApp()
        {
            this.CleanUp();
        }

        #endregion

        #region FileDumping Method

        /// <summary>
        /// Метод, выгружающий всё содержимое файла в эквивалентную программную сущность.
        /// </summary>
        /// <param name="path">Полный путь к файлу.</param>
        /// <returns>Результат считывания файла.</returns>
        public bool LoadFileContent(string path)
        {

            //Клизьмируем Контейнер для данных
            if (FileContent != null)
                FileContent.Dispose();
            FileContent = new DataSet(System.IO.Path.GetFileNameWithoutExtension(path));
        //////////////////////////////////////////////////////
            //bool bInCache = false;  //флаг "в кеше"
            //int cachedIndex = -1;   //индекс в массиве кэш. файлов

            ////Если кеш не пуст
            //if (_filesPaths.Count > 0)
            //{
            //    //Шерстим кеш
            //    foreach (string filePath in _filesPaths)
            //    {
            //        if (System.IO.Path.GetFileNameWithoutExtension(filePath) == System.IO.Path.GetFileNameWithoutExtension(path))
            //        {
            //            //Бинго
            //            bInCache = true;
            //            cachedIndex = _filesPaths.IndexOf(filePath);
            //            break;
            //        }
            //    }
            //}

            ////Если нашли
            //if (bInCache)
            //{
            //    //Возвращаем из кэша и не паримся
            //    FileContent = _cachedFiles[cachedIndex];

            //    return true;
            //}
            ////Если же нет
            //else
            //{
        //////////////////////////////////////////////////////
                try
                {
                    //Инициализируем-открываем
                    _excelApp = new Excel.Application();
                    _xWorkBook = _excelApp.Workbooks.Open(
                        path, 
                        Type.Missing, 
                        true, 
                        Type.Missing, 
                        Type.Missing, 
                        Type.Missing, 
                        Type.Missing, 
                        Type.Missing, 
                        Type.Missing, 
                        false);
                    _isOpened = true;

                    //Для каждого листа в книге.
                    foreach (Excel.Worksheet sheet in _xWorkBook.Worksheets)
                    {
                        //Узнаём границы
                        int rows = sheet.UsedRange.Rows.Count;
                        int cols = sheet.UsedRange.Columns.Count;

                        System.Data.DataTable dt = new System.Data.DataTable(sheet.Name);

                        //Создаём заголовки колонок
                        for (int iCols = 1; iCols <= cols; iCols++)
                        {
                            dt.Columns.Add(iCols.ToString());
                        }

                        //Флаг конца полезного содержимого
                        bool EOF = false;

                        //Дампим по-клеточно содержимое.
                        for (int iRows = 1; iRows <= rows; iRows++)
                        {
                            if (!EOF)
                            {
                                System.Data.DataRow dr = dt.NewRow();
                                for (int iCols = 1; iCols <= cols; iCols++)
                                {
                                    //Заносим значение ячейки
                                    dr[iCols - 1] = sheet.Cells[iRows, iCols].Value;

                                    //Проверяем на признак конца полезного содержимого
                                    if (
                                       ( (iCols == 1) && dr[0].ToString().StartsWith("Протокол засідання кафедри") ) ||
                                       ( (iCols == 1) && dr[0].ToString().StartsWith("Викладач _________________") ) ||
                                       ( (iCols == 2) && dr[1].ToString().StartsWith(" Затверджено"))
                                    )
                                    {
                                        EOF = true;
                                        break;
                                    }
                                }
                                dt.Rows.Add(dr);
                            }
                            else
                                break;
                        }
                        FileContent.Tables.Add(dt);
                    }

        //////////////////////////////////////////////////////
                    //CacheXls(FileContent, path);
        //////////////////////////////////////////////////////

                    //Закрывает-выходим
                    CleanUp();

                    return true;
                }
                catch (Exception exc)
                {
                    System.Windows.Forms.MessageBox.Show(
                        exc.Message +
                        "\n=================================\n" +
                        exc.StackTrace
                    );

                    //Закрывает-выходим
                    CleanUp();

                    return false;
                }
                finally
                {
                    //Закрывает-выходим
                    CleanUp();
                }
        //////////////////////////////////////////////////////
           // }
        //////////////////////////////////////////////////////
        }

        //Закрыватор-освобождатор
        private void CleanUp()
        {
            if (_isOpened)
            {
                //Собираем мусор
                GC.Collect();
                GC.WaitForPendingFinalizers();

                //Освобождаем память книги
                _xWorkBook.Close();
                Marshal.ReleaseComObject(_xWorkBook);

                //Освобождаем память приложения
                _excelApp.Quit();
                Marshal.ReleaseComObject(_excelApp);

                //Больше не открыто
                _isOpened = false;
            }
        }

        #endregion

        #region Cache-related

        //сюда сгружаются кэшированные файлы.
        private List<System.Data.DataSet> _cachedFiles;

        //рабочая дирректория приложения
        private string _currDir = "";

        //пути кэшированных файлов
        private List<string> _filesPaths;

        //При инициализации проверяет наличие папки кэша, создаёт при отсутсвии и 
        //если сущ. проверяет наличие файлов и если существуют - грузит в программу.
        private void CacheInit()
        {
            //Определение рабочей директории и создание папки cached
            _currDir = System.IO.Directory.GetCurrentDirectory();
            if (!System.IO.Directory.Exists(_currDir + "\\cached"))
            {
                System.IO.Directory.CreateDirectory(System.IO.Directory.GetCurrentDirectory() + "\\cached");
            }

            //Загрузка списка путей кэшированных файлов 
            _filesPaths = new List<string>(System.IO.Directory.GetFiles(_currDir + "\\cached"));

            //Де-кэширование в программные сущности.
            if (_filesPaths.Count > 0)
            {
                _cachedFiles = new List<DataSet>(_filesPaths.Count);

                for (int i = 0; i < _filesPaths.Count; i++)
                {
                    _cachedFiles.Add(DeCacheXls(_filesPaths[i]));
                }
            }
            else
            {
                _cachedFiles = new List<DataSet>();
            }
        }


        //cache dataset into binary
        private bool CacheXls(System.Data.DataSet dataToCache, string filePath)
        {
            // <директория>\\<файл>.xls(s) ==> <рабочая_директория>\\<cached>\\<файл>.bin
            string fileName = System.IO.Path.GetFileName(System.IO.Path.ChangeExtension(filePath, "bin"));
            string _cachePath = _currDir + "\\cached\\" + fileName;

            // Сериализация.
            using (System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(_cachePath))
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                try
                {
                    binaryFormatter.Serialize(streamWriter.BaseStream, FileContent);

                    //Если это новый файл, то добавляем его в список кэша и в программный буфер.
                    if (!_filesPaths.Contains(filePath))
                    {
                        _filesPaths.Add(filePath);
                        _cachedFiles.Add(dataToCache);
                    }

                    return true;
                }
                catch (System.Runtime.Serialization.SerializationException ex)
                {
                    throw new System.Runtime.Serialization.SerializationException(ex.ToString() + "\n" + ex.Source);
                }
            }
        }


        //de-cache binary into dataset
        private System.Data.DataSet DeCacheXls(string filePath)
        {
            System.Data.DataSet deserializedResult = new System.Data.DataSet();

            // Десериализация.
            using (System.IO.StreamReader streamReader = new System.IO.StreamReader(filePath))
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                try
                {
                    deserializedResult = (System.Data.DataSet)binaryFormatter.Deserialize(streamReader.BaseStream);
                }
                catch (System.Runtime.Serialization.SerializationException ex)
                {
                    throw new System.Runtime.Serialization.SerializationException(ex.ToString() + "\n" + ex.Source);
                }
                
            }

            return deserializedResult;
        }

        #endregion
    }
}

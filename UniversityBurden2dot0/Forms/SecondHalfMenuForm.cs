﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

using UniversityBurden2dot0.DataModels;

namespace UniversityBurden2dot0.Forms
{
    /// <summary>
    /// Окно формирования второй половины дня.
    /// </summary>
    public partial class SecondHalfMenuForm : Form
    {
        #region Fields

        /// <summary>
        /// Модель, на основе которой сформируется вторая половина дня.
        /// </summary>
        private DataModel _modelToFormFrom;

        /// <summary>
        /// Перечень типов работ.
        /// </summary>
        private List<string> _workTypes;

        private bool _exportCanceled = false;

        #endregion

        #region Constructor and Loading

        /// <summary>
        /// Конструктор формы.
        /// </summary>
        /// <param name="model">Модель, на основе которой сформируется вторая половина дня.</param>
        public SecondHalfMenuForm(DataModel model)
        {
            InitializeComponent();

            //Проверяем не подсунули ли нам нул
            if (model != null)
            {
                //Привязываем модель
                _modelToFormFrom = model;

                #region MainGrid

                #region Список видов работ и индекс для "прохода" по ним
                int workTypeindex = 0;
                _workTypes = new List<string>()
                {
                    //Навчально-методична робота
                    "Розробка робочих програм з дисципліни",
                    "Переробка робочих навчальних програм",
                    "Переробка програм на виробничу практику",
                    "Розробка та підготовка до видання (друкованих) електроних підручників",
                    "Підготовка навчально-методичних матеріалів для видання",
                    "Підготовка до проведення лекцій для мультимедійної аудиторії",

                    "Підготовка до проведення лабораторних робіт",
                    "Розробка та впровадження нових лабораторних робіт з використанням існуючого ПЗ",
                    "Розробка та впровадження нових лабораторних робіт з використанням нового ПЗ",

                    "Складання екзаменаційних білетів",
                    "Складання комплекту типових контрольних завданнь з навчальної дисципліни",

                    "Використання інформаційних технологій при керуванні дипломним проектуванням",
                    "Освоєння нових програмних оболонок",

                    "Розробка компютерних тестів з навчальної дисципліни",
                    "Доопрацьовування тестів (0,5 від розробки)",

                    //Розробка завдань
                    "Розробка завдань на дипломний проект",
                    "Розробка завдань на бакалаврську роботу",
                    "Розробка завдань на курсовий проект",
                    "Розробка завдань на домашню роботу",

                    //Наукова робота
                    "Написання статей",
                    "Підготовка тез доповідей",
                    "Керівництво науковою роботою студентів з підготовкою доповіді на конференцію",
                    "Пошук електроних джерел",

                    //Виховна робота
                    "Керівництво студентським гуртком",
                    "Виконання обов’язків куратора"
                };
                #endregion

                #region Добавление строк
                foreach (var item in _workTypes)
                {
                    DataGridViewRow row = new DataGridViewRow
                    { Resizable = DataGridViewTriState.False };

                    row.Cells.Add(new DataGridViewTextBoxCell { Value = item });
                    row.Cells.Add(new DataGridViewTextBoxCell { Value = 0 });
                    row.Cells.Add(new DataGridViewTextBoxCell { Value = 0 });
                    row.Cells.Add(new DataGridViewTextBoxCell { Value = 0 });
                    row.Cells.Add(new DataGridViewTextBoxCell { Value = 0 });
                    row.Cells.Add(new DataGridViewTextBoxCell { Value = " " });

                    MainGridView.Rows.Add(row);
                }
                #endregion

                #region Настройка мин.макс.
                //Розробка робочих програм з дисципліни
                MainGridView[2, workTypeindex].Value = 0;
                MainGridView[4, workTypeindex].Value = _modelToFormFrom.Subjects.Count * 50;
                workTypeindex++;

                //Переробка робочих навчальних програм
                MainGridView[2, workTypeindex].Value = 0;
                MainGridView[4, workTypeindex].Value = _modelToFormFrom.Subjects.Count * 30;
                workTypeindex++;

                //Переробка програм на виробничу практику
                MainGridView[2, workTypeindex].Value = 0;
                MainGridView[4, workTypeindex].Value = _modelToFormFrom.Subjects.Count * 30;
                workTypeindex++;

                //Розробка та підготовка до видання (друкованих) електроних підручників
                MainGridView[2, workTypeindex].Value = 0;
                MainGridView[4, workTypeindex].Value = "-";
                workTypeindex++;

                //Підготовка навчально-методичних матеріалів для видання
                MainGridView[2, workTypeindex].Value = 0;
                MainGridView[4, workTypeindex].Value = "-";
                workTypeindex++;

                //Підготовка до проведення лекцій для мультимедійної аудиторії
                MainGridView[2, workTypeindex].Value = 0;
                MainGridView[4, workTypeindex].Value = _modelToFormFrom.Hours["Лекції"] * 16;
                workTypeindex++;


                //Підготовка до проведення лабораторних робіт
                MainGridView[2, workTypeindex].Value = 0;
                MainGridView[4, workTypeindex].Value = _modelToFormFrom.Hours["Лабораторні Роботи"]/2;
                workTypeindex++;

                //Розробка та впровадження нових лабораторних робіт з використанням існуючого ПЗ
                MainGridView[2, workTypeindex].Value = 0;
                MainGridView[4, workTypeindex].Value = "-";
                workTypeindex++;

                //Розробка та впровадження нових лабораторних робіт з використанням нового ПЗ
                MainGridView[2, workTypeindex].Value = 0;
                MainGridView[4, workTypeindex].Value = "-";
                workTypeindex++;


                //Складання екзаменаційних білетів
                if (_modelToFormFrom.Hours["ЕК"] > 0)
                {
                    MainGridView[2, workTypeindex].Value = _modelToFormFrom.StudentFlows.Count * 10;
                    MainGridView[4, workTypeindex].Value = _modelToFormFrom.StudentFlows.Count * 20;
                }
                workTypeindex++;

                //Складання комплекту типових контрольних завданнь з навчальної дисципліни 
                MainGridView[2, workTypeindex].Value = _modelToFormFrom.Subjects.Count * 5;
                MainGridView[4, workTypeindex].Value = _modelToFormFrom.Subjects.Count * 10;
                workTypeindex++;


                //Розробка компютерних тестів з навчальної дисципліни
                MainGridView[2, workTypeindex].Value = 0;
                MainGridView[4, workTypeindex].Value = "-";
                workTypeindex++;

                //Доопрацьовування тестів (0,5 від розробки)
                MainGridView[2, workTypeindex].Value = 0;
                MainGridView[4, workTypeindex].Value = "-";
                workTypeindex++;

                //Використання інформаційних технологій при керуванні дипломним проектуванням
                MainGridView[2, workTypeindex].Value = 0;
                MainGridView[4, workTypeindex].Value = "-";
                workTypeindex++;


                //Розробка завдань на дипломний проект
                MainGridView[2, workTypeindex].Value = 0;
                MainGridView[4, workTypeindex].Value = "-";
                workTypeindex++;

                //Розробка завдань на бакалаврську роботу
                MainGridView[2, workTypeindex].Value = 0;
                MainGridView[4, workTypeindex].Value = "-";
                workTypeindex++;

                //Розробка завдань на курсовий проект
                MainGridView[2, workTypeindex].Value = 0;
                MainGridView[4, workTypeindex].Value = "-";
                workTypeindex++;

                //Розробка завдань на домашню роботу
                MainGridView[2, workTypeindex].Value = 0;
                MainGridView[4, workTypeindex].Value = "-";
                workTypeindex++;


                //Освоєння нових програмних оболонок
                MainGridView[2, workTypeindex].Value = 0;
                MainGridView[4, workTypeindex].Value = "-";
                workTypeindex++;


                //Написання статей
                MainGridView[2, workTypeindex].Value = 0;
                MainGridView[4, workTypeindex].Value = "-";
                workTypeindex++;

                //Підготовка тез доповідей
                MainGridView[2, workTypeindex].Value = 0;
                MainGridView[4, workTypeindex].Value = "-";
                workTypeindex++;

                //Керівництво науковою роботою студентів з підготовкою доповіді на конференцію
                MainGridView[2, workTypeindex].Value = 0;
                MainGridView[4, workTypeindex].Value = "-";
                workTypeindex++;

                //Пошук електроних джерел
                MainGridView[2, workTypeindex].Value = 0;
                MainGridView[4, workTypeindex].Value = "-";
                workTypeindex++;


                //Керівництво студентським гуртком
                MainGridView[2, workTypeindex].Value = 0;
                MainGridView[4, workTypeindex].Value = 50;
                workTypeindex++;

                //Виконання обов’язків куратора
                MainGridView[2, workTypeindex].Value = 0;
                MainGridView[4, workTypeindex].Value = 100;
                workTypeindex++;

                #endregion

                #endregion
            }
            //Либо ругаемся и шлём юзера
            else
            {
                MessageBox.Show("Passed model is null!", "Error", MessageBoxButtons.OK);
                this.DialogResult = DialogResult.Abort;
                this.Close();
            }
        }

        /// <summary>
        /// Обрабативает загрузку формы
        /// </summary>
        private void SecondHalfMenuForm_Load(object sender, EventArgs e)
        {
            this.Icon = Properties.Resources.edit_icon;
            this.ActiveControl = HoursTotalTextBox;

            UpdateHeaderHours();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Обработка закрытия
        /// </summary>
        private void SecondHalfMenuForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(e.CloseReason == CloseReason.UserClosing && this.DialogResult != DialogResult.Cancel)
            { this.DialogResult = DialogResult.OK; }
        }

        /// <summary>
        /// Обработка смены кол-ва общых часов.
        /// </summary>
        private void HoursTotalTextBox_TextChanged(object sender, EventArgs e)
        {
            UpdateHeaderHours();
        }

        /// <summary>
        /// Не допускает ввод любых символов кроме цыфр.
        /// </summary>
        private void HoursTotalTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Окрашивает в красный цвет текст поля, если оно отрицательно
        /// </summary>
        private void HoursSecondHalfTotalTextBox_TextChanged(object sender, EventArgs e)
        {
            if (int.Parse(HoursSecondHalfTotalTextBox.Text) < 0)
            {
                HoursSecondHalfTotalTextBox.ForeColor = Color.Red;
                AutoSecondHalfButton.Enabled = false;
            }
            else
            {
                HoursSecondHalfTotalTextBox.ForeColor = SystemColors.WindowText;
                AutoSecondHalfButton.Enabled = true;
            }
        }

        /// <summary>
        /// Следит за форматом и пересчитвает занятые часы.
        /// </summary>
        private void MainGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //Если находимся в колонке выделенных часов
            if (MainGridView.Columns[e.ColumnIndex].Name == "AllocatedHoursColumn")
            {
                //Если пользовательно всё не стёр
                if (MainGridView[e.ColumnIndex, e.RowIndex].Value != null)
                {
                    //Если текст в клетке содержит только цыфры
                    if (MainGridView[e.ColumnIndex, e.RowIndex].Value.ToString().All(c => c >= '0' && c <= '9'))
                    {
                        //Вычисляем часы
                        int intValue = int.Parse((string)MainGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value);
                        int minValue = (int)MainGridView.Rows[e.RowIndex].Cells[e.ColumnIndex + 1].Value;
                        int maxValue = 0;
                        if (MainGridView[e.ColumnIndex + 3, e.RowIndex].Value.ToString() == "-")
                        {
                            maxValue = 999;
                        }
                        else
                        {
                            maxValue = (int)MainGridView.Rows[e.RowIndex].Cells[e.ColumnIndex + 3].Value;
                        }

                        bool inRange = false;

                        //Если не дефолтное значение
                        if (intValue != 0)
                        {
                            //Проверяем на не-вхождение в допустимый диапазон
                            if (maxValue != 999)
                            {
                                inRange = ((intValue >= minValue) && //Вхождение в мин. порог
                                           (intValue <= maxValue));  //Вхождение в макс. порог
                            }
                            else
                            { inRange = true; }
                        }
                        else
                        { inRange = true; }

                        //Определяем (не)вхождение
                        if(inRange)
                        {
                            //Вошли -- красим клетку в норм цвет и обновляем часы
                            MainGridView[e.ColumnIndex, e.RowIndex].Style.BackColor = SystemColors.Window;
                            UpdateHeaderHours(false);
                        }
                        else
                        {
                            //Не вошли -- красим клетку в красный
                            MainGridView[e.ColumnIndex, e.RowIndex].Style.BackColor = Color.Red;
                        }
                    }
                    //Если содержит не только цыфры
                    else
                    {
                        //Красим клетку в красный
                        MainGridView[e.ColumnIndex, e.RowIndex].Style.BackColor = Color.Red;
                    }
                }
                //Если таки стёр
                else
                {
                    //Записываем ноль
                    MainGridView[e.ColumnIndex, e.RowIndex].Value = 0;
                }
            }
            //Остальное игнорируем
            else if (MainGridView.Columns[e.ColumnIndex].Name == "CommentColumn")
            { }
            else return;
        }

        #region Buttons

        /// <summary>
        /// Закрывает форму с результом "Отмена".
        /// </summary>
        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        /// <summary>
        /// Вызывает вычисление рекомендованых значений часов.
        /// </summary>
        private void AutoSecondHalfButton_Click(object sender, EventArgs e)
        {
            EvaluateRecommendedHours();
        }

        private void ExportToDocButton_Click(object sender, EventArgs e)
        {
            bool exportResult = this.ExportToDoc();

            if (!_exportCanceled)
            {
                MessageBox.Show(
                    (exportResult) ? "Дані успішно експортовані." : "Під час експорту даних виникла помилка!",
                    "",
                    MessageBoxButtons.OK);
            }
        }

        #endregion

        #endregion

        #region Logic

        /// <summary>
        /// Вычисляет коллиства часов в хедере.
        /// </summary>
        private void UpdateHeaderHours(bool updateAll = true)
        {
            //Вычисляем часы первой половины дня
            int firstHalfHours = 0;
            foreach (var hoursType in _modelToFormFrom.Hours.Values)
            {
                firstHalfHours += hoursType;
            }

            //Вычисляем общее число часов второй половины дня
            int secondHalfHours = int.Parse(HoursTotalTextBox.Text) - firstHalfHours;

            if (updateAll)
            {
                HoursFirstHalfTextBox.Text = firstHalfHours.ToString();
                HoursSecondHalfTotalTextBox.Text = secondHalfHours.ToString();
            }

            //Вычисляем занятое число часов второй половины дня
            int secondHalfHoursOccupied = 0;
            foreach (DataGridViewRow row in MainGridView.Rows)
            {
                if(int.Parse(row.Cells[1].Value.ToString()) != 0)
                {
                    secondHalfHoursOccupied += int.Parse(row.Cells[1].Value.ToString());
                }
            }
            HoursSecondHalfLeftTextBox.Text = secondHalfHoursOccupied.ToString();

            //Меняем состояние в зависимости от того попадаем ли в часы
            if(secondHalfHours == secondHalfHoursOccupied)
            {
                ExportToDocButton.Enabled = true;
                HoursSecondHalfLeftTextBox.ForeColor = Color.Green;
            }
            else
            {
                ExportToDocButton.Enabled = false;
                HoursSecondHalfLeftTextBox.ForeColor = SystemColors.WindowText;
            }
            
        }

        /// <summary>
        /// Вычисляет рекомендованное число часов и заполняет GridView.
        /// </summary>
        private void EvaluateRecommendedHours()
        {
            foreach(DataGridViewRow row in MainGridView.Rows)
            {
                if(row.Cells[4].Value.ToString() != "-")
                {
                    row.Cells[3].Value = ((int)row.Cells[2].Value + (int)row.Cells[4].Value) / 2;
                }
                else
                {
                    row.Cells[3].Value = "-";
                }
            }
        }

        /// <summary>
        /// Экспортирует сформированные данные в doc документ.
        /// </summary>
        /// <returns></returns>
        private bool ExportToDoc()
        {
            //Выбираем куда сохранить инд. план.
            SaveFileDialog dlg = new SaveFileDialog
            {
                FileName = _modelToFormFrom.Title.FileName + " ИНД ПЛАН",
                RestoreDirectory = true,
                DefaultExt = ".docx",
                Filter = "Documents (.docx)|*.docx"
            };

            //Если юзер закрыл диалог
            if (dlg.ShowDialog() == DialogResult.Cancel)
            {
                //возвращает соотв. значение
                _exportCanceled = true;
                return true;
            }
            //Если выбрал место
            else
            {
                //Запоминем пусть сохранения
                string path = dlg.FileName;

                //Упаковываем нужные данные
                List<Tuple<string, int, string>> data = new List<Tuple<string, int, string>>();
                foreach (DataGridViewRow row in MainGridView.Rows)
                {
                    if(int.Parse(row.Cells[1].Value.ToString()) > 0)
                    {
                        data.Add(
                            new Tuple<string, int, string>(
                                    row.Cells[0].Value.ToString(),
                                    int.Parse(row.Cells[1].Value.ToString()),
                                    row.Cells[5].Value.ToString()
                                )
                            );
                    }
                }

                //Оправляем хэндлеру на экспорт
                return Handlers.DOCHandler.StaticExportToDoc(path, data);
            }
        }

        #endregion
    }
}

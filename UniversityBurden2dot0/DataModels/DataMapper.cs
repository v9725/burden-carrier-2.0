﻿using System;
using System.Collections.Generic;

namespace UniversityBurden2dot0.DataModels
{
    /// <summary>
    /// Класс для обеспечения целостности, унификации формы данных и взаимосвязи Модели, БД и Отображения.
    /// </summary>
    public class DataMapper
    {
        #region Constuctor

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        public DataMapper()
        { }

        #endregion

        #region Raw DataSet Processing

        /// <summary>
        /// Метод, перерабатывающий сырые данные из файла в соотв. структурированную модель.
        /// </summary>
        /// <param name="rawDataSet">Ссылка на сырые данные.</param>
        /// <returns>Статус обработки: (не)удача.</returns>
        public DataModel ProccessRawDataSet(System.Data.DataSet rawDataSet)
        {
            //Проверяем не подсунули ли нам нул
            if (rawDataSet != null)
            {
                //Определяем тип нагрузки и обрабатываем соответственно
                return (rawDataSet.Tables.Count > 1) ?
                    TwoPage(rawDataSet) :
                    OnePage(rawDataSet);
            }
            else
            { throw new Exception("Null DataSet Exception"); }
        }

        #region Sub-Parsers

        /// <summary>
        /// Обрабатывает одностраничную нагрузку.
        /// </summary>
        /// <param name="rawDataSet">Ссылка на сырые данные.</param>
        /// <returns>Статус обработки: (не)удача.</returns>
        private DataModel OnePage(System.Data.DataSet rawDataSet)
        {
            DataModel resultModel = new DataModel(new FileTitle(DateTime.Now.ToString("yyyy.MM.dd"), rawDataSet.DataSetName));
            
            //Данные с которыи будем работать
            var RowsRange = rawDataSet.Tables[0].Rows;

            #region Teacher string
            string teacher = "";

            //Получаем строку с ФИО преподавателя
            teacher = RowsRange[3][0].ToString();

            //Определяем что отсекать
            int indexOfStart = teacher.IndexOf('(') + 1;
            int indexOfEnd = teacher.IndexOf(')');

            //Добавляем в данные нужную подстроку -- ФИО преподавателя
            teacher = teacher.Substring(indexOfStart, (indexOfEnd - indexOfStart));
            resultModel.Teachers.Add(teacher);
            #endregion

            #region Parsing meta-data
            //Начальный индекс парсинга -- первая строка нагрузки, сразу под строкой с "1 | 2 | 3 | 4 | 5..."
            int ParsingIndex = 10;
            //Индекс внутри модели
            int ModelIndex = -1;

            //Семестр
            int CurrentSemester = 1;

            //флаг прохождения семестров
            bool SemestersParsed = false;
            #endregion

            #region Parsing cycle
            //Парсинговый цикл
            do
            {
                if (!SemestersParsed)
                {
                    //Если начало первого семестра
                    if (RowsRange[ParsingIndex][0].ToString() == "Осінній семестр")
                    {
                        //Выставляем семестр
                        CurrentSemester = 1;

                        //Идёт дальше
                        ++ParsingIndex;
                        continue;
                    }
                    //Если конец первого семестра или начало второго
                    else if (
                        RowsRange[ParsingIndex][0].ToString() == "Усього за осінній семестр" ||
                        RowsRange[ParsingIndex][0].ToString() == "Весняний семестр")
                    {
                        //Выставляем семестр
                        CurrentSemester = 2;

                        //Идём дальше
                        ++ParsingIndex;
                        continue;
                    }
                    //Если конец второго семестра
                    else if (RowsRange[ParsingIndex][0].ToString() == "Усього за весняний семестр")
                    {
                        //Выставляем флаг
                        SemestersParsed = true;

                        //Идём дальше
                        ++ParsingIndex;
                        continue;
                    }
                    else
                    {
                        //Предмет
                        string subject = RowsRange[ParsingIndex][0].ToString();
                        if (!resultModel.Subjects.Contains(subject)) { resultModel.Subjects.Add(subject); }

                        //Поток
                        string flow = RowsRange[ParsingIndex][1].ToString();
                        if (!resultModel.StudentFlows.Contains(flow)) { resultModel.StudentFlows.Add(flow); }

                        //Если в потоке не одна группа
                        if (flow.Contains(", "))
                        {
                            //Разбиваем название на группы
                            string[] groups = FlowToGroups(flow);
                            for (int i = 0; i < groups.Length; i++)
                            {
                                //Добавляем в список груп
                                if (!resultModel.Groups.Contains(groups[i])) { resultModel.Groups.Add(groups[i]); }
                            }
                        }
                        //В противном случае
                        else
                        {
                            //Добавляем в список груп
                            if (!resultModel.Groups.Contains(flow)) { resultModel.Groups.Add(flow); }
                        }

                        //Проходимся до конца строки
                        for (int i = 4; i < 16; i++)
                        {
                            string studyType = "";
                            try
                            {
                                //Вид учебной работы
                                studyType = UnifyStudyType(RowsRange[8][i].ToString());
                                if (!resultModel.StudyTypes.Contains(studyType)) { resultModel.StudyTypes.Add(studyType); }

                                //Создаём новую запись
                                resultModel.LoadEntries.Add(
                                    new LoadEntryItem(
                                        CurrentSemester,
                                        subject,
                                        teacher,
                                        studyType,
                                        flow,
                                        int.Parse(RowsRange[ParsingIndex][i].ToString())
                                    )
                                );
                                ++ModelIndex;
                            }
                            catch (Exception exc)
                            {
                                System.Windows.Forms.MessageBox.Show(exc.Message + "\n=============================\n" + exc.StackTrace);
                                return null;
                            }
                            //Накапливаем общие часы
                            resultModel.Hours[studyType] += resultModel.LoadEntries[ModelIndex].Hours;
                        }
                    }
                }

                //Увеличиваем индекс
                ++ParsingIndex;

                //Выходим из цикла когда долшли до конца таблицы
            } while (RowsRange[ParsingIndex][0].ToString() != "Усього за рік");
            #endregion

            return resultModel;
        }

        /// <summary>
        /// Обрабатывает двухстраничную нагрузку.
        /// </summary>
        /// <param name="rawDataSet">Ссылка на сырые данные.</param>
        /// <returns>Статус обработки: (не)удача.</returns>
        private DataModel TwoPage(System.Data.DataSet rawDataSet)
        {
            DataModel resultModel = new DataModel(new FileTitle(DateTime.Now.ToString("yyyy.MM.dd"), rawDataSet.DataSetName));

            #region Teacher String
            //Получаем строку с ФИО преподавателя
            string teacher = rawDataSet.DataSetName;

            //Определяем что отсекать
            int indexOfStart = teacher.IndexOf('_') + 1;
            int indexOfEnd = teacher.IndexOf('(') - 1;

            //Добавляем в данные -- ФИО преподавателя
            teacher = teacher.Substring(indexOfStart, (indexOfEnd - indexOfStart));
            resultModel.Teachers.Add(teacher);
            #endregion

            #region Parsing meta-data
            //Начальный индекс парсинга -- первая строка нагрузки, сразу под строкой с "1 | 2 | 3 | 4 | 5..."
            int ParsingIndex = 5;
            //Индекс внутри модели
            int ModelIndex = -1;

            //Семестр
            int CurrentSemester = 1;

            //флаг прохождения семестров
            bool SemestersParsed = false;
            #endregion

            #region Parsing cycle
            do
            {
                if (!SemestersParsed)
                {
                    //Если достигли конца первого семестра
                    if (rawDataSet.Tables[0].Rows[ParsingIndex][0].ToString() == "Разом за I семестр")
                    {
                        //Выставляем семестр
                        CurrentSemester = 2;

                        //Идём дальше
                        ++ParsingIndex;
                        continue;
                    }
                    //Если достигли конца второго семестра
                    else if (rawDataSet.Tables[0].Rows[ParsingIndex][0].ToString() == "Разом за II семестр")
                    {
                        //Выставляем флаг
                        SemestersParsed = true;

                        //Идём дальше
                        ++ParsingIndex;
                        continue;
                    }
                    else
                    {
                        //Предмет
                        string subject = rawDataSet.Tables[0].Rows[ParsingIndex][1].ToString();
                        if (!resultModel.Subjects.Contains(subject)) { resultModel.Subjects.Add(subject); }

                        //Поток
                        string flow = rawDataSet.Tables[0].Rows[ParsingIndex][5].ToString();
                        if (!resultModel.StudentFlows.Contains(flow)) { resultModel.StudentFlows.Add(flow); }

                        //Если в потоке не одна группа
                        if (flow.Contains(", "))
                        {
                            //Разбиваем название на группы
                            string[] groups = FlowToGroups(flow);
                            for (int i = 0; i < groups.Length; i++)
                            {
                                //Добавляем в список груп
                                if (!resultModel.Groups.Contains(groups[i])) { resultModel.Groups.Add(groups[i]); }
                            }
                        }
                        //В противном случае
                        else
                        {
                            //Добавляем в список груп
                            if (!resultModel.Groups.Contains(flow)) { resultModel.Groups.Add(flow); }
                        }

                        int tableIndex = 0;
                        int innerI = 0;
                        int consHours = 0;

                        //Проходимся до конца строки (через два листа)
                        for (int i = 6; i < 34; i += 2)
                        {
                            //Переключение страниц
                            if (i < 13)
                            {
                                tableIndex = 0;
                                innerI = i;
                            }
                            else
                            {
                                tableIndex = 1;
                                innerI = i - 14;//Вычисление индекса на второй странице
                            }

                            string studyType = "";
                            try
                            {
                                //Вид учебной работы
                                studyType = rawDataSet.Tables[tableIndex].Rows[1][innerI].ToString();

                                //Исключения
                                //Игнор индивидуалок
                                if (studyType == "Проведення індивідуальних занять") continue;
                                //Запоминание часов первой индивидуалки для влива во вторую
                                if (studyType == "Проведення консультацій з навчальних дисциплін протягом семестру")
                                {
                                    consHours = int.Parse(rawDataSet.Tables[tableIndex].Rows[ParsingIndex][innerI].ToString());
                                    continue;
                                }

                                //Унификация
                                studyType = UnifyStudyType(studyType);

                                //Занесение в модель
                                if (!resultModel.StudyTypes.Contains(studyType)) { resultModel.StudyTypes.Add(studyType); }

                                //Создаём новую запись
                                resultModel.LoadEntries.Add(
                                    new LoadEntryItem(
                                        CurrentSemester,
                                        subject,
                                        teacher,
                                        studyType,
                                        flow,
                                        int.Parse(rawDataSet.Tables[tableIndex].Rows[ParsingIndex][innerI].ToString())
                                        )
                                    );
                                ++ModelIndex;

                                //Сливаем часы консультаций в одну запись
                                if (consHours != 0)
                                {
                                    resultModel.LoadEntries[ModelIndex].Hours += consHours;
                                    consHours = 0;
                                }
                            }
                            catch (Exception exc)
                            {
                                System.Windows.Forms.MessageBox.Show(exc.Message + "\n=============================\n" + exc.StackTrace);
                                return null;
                            }

                            //Накапливаем общие часы
                            resultModel.Hours[studyType] += resultModel.LoadEntries[ModelIndex].Hours;
                        }
                    }
                }

                //Увеличиваем индекс
                ++ParsingIndex;

                //Выходим из цикла когда долшли до конца таблицы
            } while (rawDataSet.Tables[0].Rows[ParsingIndex][0].ToString() != "Всього за навчальний рік");
            #endregion

            return resultModel;
        }

        #endregion

        #endregion

        #region Doc(x) Export

        //Формирует файл.
        public bool BuildDocXFile()
        {
            return false;
        }

        /// <summary>
        /// Записывает сформированный документ на диск в указанный каталог.
        /// </summary>
        /// <param name="folderName">Полный пусть к каталогу.</param>
        /// <returns></returns>
        public bool ExportDocXFile(string folderName)
        {
            return false;
        }

        #endregion

        #region Utils

        /// <summary>
        /// Метод, который унифицирует названия видов работ.
        /// </summary>
        /// <param name="studyTypeVariant">Вариант написания типа работы.</param>
        /// <returns>Унифицированное название типа работы</returns>
        private string UnifyStudyType(string studyTypeVariant)
        {
            switch (studyTypeVariant)
            {
                #region ЛК
                case "Лекційних":
                    return "Лекції";
                case "Читання лекцій":
                    return "Лекції";
                #endregion

                #region ПР
                case "Практичних":
                    return "Практичні Роботи";
                case "Проведення практичних занять":
                    return "Практичні Роботи";
                #endregion

                #region ЛБ
                case "Лабораторних":
                    return "Лабораторні Роботи";
                case "Проведення лабораторних занять":
                    return "Лабораторні Роботи";
                #endregion

                #region Конс
                case "Консультації":
                    return "Консультації";
                case "Проведення консультацій з навчальних дисциплін протягом семестру":
                    return "Консультації";
                case "Проведення екзаменаційних консультацій":
                    return "Консультації";
                #endregion

                #region КП/КР
                case "КП/КР":
                    return "КП/КР";
                case "Керівництво і приймання курсових проектів, робіт":
                    return "КП/КР";
                #endregion

                #region КР/РГЗ
                case "Контр. робіт і РГЗ":
                    return "Контр. роботи і РГЗ";
                case "Перевірка контрольних (модульних) робіт, що виконуються під час аудиторних занять":
                    return "Контр. роботи і РГЗ";
                case "Перевірка контрольних (модульних) робіт, що виконуються під час самостійної роботи":
                    return "Контр. роботи і РГЗ";
                #endregion

                #region Семестр. кр.
                case "Семестровий контроль":
                    return "Семестровий контроль";
                case "Проведення заліку":
                    return "Семестровий контроль";
                case "Проведення семестрових екзаменів":
                    return "Семестровий контроль";
                #endregion

                #region Дипл.
                case "Дипломування":
                    return "Дипломування";
                case "Керівництво, консультування, рецензування та проведення захисту дипломних проектів (робіт)":
                    return "Дипломування";
                #endregion

                #region ЕКЗ
                case "ЕК":
                    return "ЕК";
                case "Проведення захисту дипломних проектів (робіт)":
                    return "ЕК";
                #endregion

                #region Асп.
                case "Аспірантура":
                    return "Аспірантура";
                case "Керівництво аспірантами, здобувачами та стажуванням викладачів":
                    return "Аспірантура";
                #endregion

                #region Маг.
                case "Магістратура":
                    return "Магістратура";
                #endregion

                #region Практ.
                case "Практика":
                    return "Практика";
                case "Керівництво практикою":
                    return "Практика";
                #endregion

                //Ошибка
                default:
                    return "Parsing Error";
            }
        }

        /// <summary>
        /// Разбивает строку потока формата "{0}; {1}; {2}" на массив строк шифров групп.
        /// </summary>
        /// <param name="flow">Строка названия потока формата "{0}; {1}; {2}".</param>
        /// <returns>Массив строк шифров групп.</returns>
        public static string[] FlowToGroups(string flow)
        {
            string[] groups = flow.Split(' ');
            for (int i = 0; i < groups.Length; i++)
            {
                //Убираем запятые
                if (groups[i].EndsWith(",")) { groups[i] = groups[i].TrimEnd(','); }
            }

            return groups;
        }

        #endregion
    }
}

﻿using System.Collections.Generic;
using System.Linq;

namespace UniversityBurden2dot0.DataModels
{
    /// <summary>
    /// Класс, представляющий модель данных програмы.
    /// </summary>
    public class DataModel : System.IEquatable<DataModel>
    {
        #region Fields

        /// <summary>
        /// Название файла, соответствующего модели в формате "дата_загрузки | имя_файла".
        /// </summary>
        public FileTitle Title { get; set; }

        /// <summary>
        /// Преподаватели, присутствующие в нагрузке.
        /// </summary>
        public List<string> Teachers { get; set; }

        /// <summary>
        /// Предметы присутвтующие в нагрузке.
        /// </summary>
        public List<string> Subjects { get; set; }

        /// <summary>
        /// Виды учебных работ.
        /// </summary>
        public List<string> StudyTypes { get; set; }

        /// <summary>
        /// Группы, присутствующие в нагрузке.
        /// </summary>
        public List<string> Groups { get; set; }

        /// <summary>
        /// Потоки, присутствующие в нагрузке.
        /// </summary>
        public List<string> StudentFlows { get; set; }

        /// <summary>
        /// Количество выделенных часов по видам учебных работ.
        /// </summary>
        public Dictionary<string, int> Hours { get; set; }


        /// <summary>
        /// Список нагрузки по каждому из видов работ.
        /// </summary>
        public List<LoadEntryItem> LoadEntries { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Конструктор класса. Инициализирует поля.
        /// </summary>
        /// <param name="title">Строковое представление названия.</param>
        public DataModel(string title = "")
        {
            if (title != "") Title = new FileTitle(title);
            else Title = null;

            //Инициализируем поля
            Teachers = new List<string>();
            Subjects = new List<string>();
            StudyTypes = new List<string>();
            Groups = new List<string>();
            StudentFlows = new List<string>();
            Hours = new Dictionary<string, int>();

            LoadEntries = new List<LoadEntryItem>();

            //Заполняем типы работ.
            Hours.Add("Лекції", 0);
            Hours.Add("Практичні Роботи", 0);
            Hours.Add("Лабораторні Роботи", 0);
            Hours.Add("Консультації", 0);
            Hours.Add("КП/КР", 0);
            Hours.Add("Контр. роботи і РГЗ", 0);
            Hours.Add("Семестровий контроль", 0);
            Hours.Add("Дипломування", 0);
            Hours.Add("ЕК", 0);
            Hours.Add("Аспірантура", 0);
            Hours.Add("Магістратура", 0);
            Hours.Add("Практика", 0);
        }

        /// <summary>
        /// Конструктор класса. Инициализирует поля.
        /// </summary>
        /// <param name="title">Классовое представление названия.</param>
        public DataModel(FileTitle title = null)
        {
            Title = title;
            
            //Инициализируем поля
            Teachers = new List<string>();
            Subjects = new List<string>();
            StudyTypes = new List<string>();
            Groups = new List<string>();
            StudentFlows = new List<string>();
            Hours = new Dictionary<string, int>();

            LoadEntries = new List<LoadEntryItem>();

            //Заполняем типы работ.
            Hours.Add("Лекції", 0);
            Hours.Add("Практичні Роботи", 0);
            Hours.Add("Лабораторні Роботи", 0);
            Hours.Add("Консультації", 0);
            Hours.Add("КП/КР", 0);
            Hours.Add("Контр. роботи і РГЗ", 0);
            Hours.Add("Семестровий контроль", 0);
            Hours.Add("Дипломування", 0);
            Hours.Add("ЕК", 0);
            Hours.Add("Аспірантура", 0);
            Hours.Add("Магістратура", 0);
            Hours.Add("Практика", 0);
        }

        #endregion

        #region Methods

        //Производит сравнение без учёта названия
        public bool CompareContent(DataModel other)
        {
            if (!ReferenceEquals(this, other))
            {
                if (ReferenceEquals(null, other))
                {
                    return false;
                }
                else
                {
                    bool result =
                        this.Hours.OrderBy(i => i.GetHashCode()).SequenceEqual(other.Hours.OrderBy(i => i.GetHashCode())) &&
                        this.Teachers.OrderBy(i => i?.GetHashCode()).SequenceEqual(other.Teachers.OrderBy(i => i?.GetHashCode())) &&
                        this.Subjects.OrderBy(i => i?.GetHashCode()).SequenceEqual(other.Subjects.OrderBy(i => i?.GetHashCode())) &&
                        this.Groups.OrderBy(i => i?.GetHashCode()).SequenceEqual(other.Groups.OrderBy(i => i?.GetHashCode())) &&
                        this.StudentFlows.OrderBy(i => i?.GetHashCode()).SequenceEqual(other.StudentFlows.OrderBy(i => i?.GetHashCode()));

                    if (result)
                    {
                        //Если отличается колличесво
                        if (this.LoadEntries.Count != other.LoadEntries.Count)
                        {
                            //Значит уже не равны
                            result = false;
                        }
                        else
                        {
                            bool foundEqualItem = false;

                            //Ищем аналог каждой записи из одной коллеции в другой
                            foreach (var thisItem in this.LoadEntries)
                            {
                                foreach (var otherItem in other.LoadEntries)
                                {
                                    //Если не нашли -- сравниваем следующую
                                    if (thisItem.ToString() != otherItem.ToString())
                                    {
                                        continue;
                                    }
                                    //Если нашли -- переходим к следующей записи верхнего списка
                                    else
                                    {
                                        foundEqualItem = true;
                                        break;
                                    }
                                }

                                //Если не нашли -- значит отличается
                                if (!foundEqualItem)
                                {
                                    result = false;
                                    break;
                                }
                            }
                        }
                    }
                    
                    return result;
                }
            }
            else
            {
                return true;
            }
        }

        public static bool operator == (DataModel model1, DataModel model2)
        {
            if (!ReferenceEquals(model1, model2))
            {
                if (ReferenceEquals(model1, null) || ReferenceEquals(model2, null))
                {
                    return false;
                }
                else
                {
                    bool result =
                        model1.Title == model2.Title &&

                        model1.Hours.OrderBy(i => i.GetHashCode()).SequenceEqual(model2.Hours.OrderBy(i => i.GetHashCode())) &&
                        model1.Teachers.OrderBy(i => i.GetHashCode()).SequenceEqual(model2.Teachers.OrderBy(i => i.GetHashCode())) &&
                        model1.Subjects.OrderBy(i => i.GetHashCode()).SequenceEqual(model2.Subjects.OrderBy(i => i.GetHashCode())) &&
                        model1.Groups.OrderBy(i => i.GetHashCode()).SequenceEqual(model2.Groups.OrderBy(i => i.GetHashCode())) &&
                        model1.StudentFlows.OrderBy(i => i.GetHashCode()).SequenceEqual(model2.StudentFlows.OrderBy(i => i.GetHashCode()));

                    if (result)
                    {
                        //Если отличается колличесво
                        if (model1.LoadEntries.Count != model2.LoadEntries.Count)
                        {
                            //Значит уже не равны
                            result = false;
                        }
                        else
                        {
                            bool foundEqualItem = false;

                            //Ищем аналог каждой записи из одной коллеции в другой
                            foreach (var thisItem in model1.LoadEntries)
                            {
                                foreach (var otherItem in model2.LoadEntries)
                                {
                                    //Если не нашли -- сравниваем следующую
                                    if (thisItem.ToString() != otherItem.ToString())
                                    {
                                        continue;
                                    }
                                    //Если нашли -- переходим к следующей записи верхнего списка
                                    else
                                    {
                                        foundEqualItem = true;
                                        break;
                                    }
                                }

                                //Если не нашли -- значит отличается
                                if (!foundEqualItem)
                                {
                                    result = false;
                                    break;
                                }
                            }
                        }
                    }

                    return result;
                }
            }
            else
            {
                return true;
            }
        }

        public static bool operator != (DataModel model1, DataModel model2)
        {
            return !(model1 == model2);
        }

        public bool Equals(DataModel other)
        {
            if (!ReferenceEquals(this, other))
            {
                if (ReferenceEquals(null, other))
                {
                    return false;
                }
                else
                {
                    bool result =
                        this.Title == other.Title &&
                        this.Hours.OrderBy(i => i.GetHashCode()).SequenceEqual(other.Hours.OrderBy(i => i.GetHashCode())) &&
                        this.Teachers.OrderBy(i => i?.GetHashCode()).SequenceEqual(other.Teachers.OrderBy(i => i?.GetHashCode())) &&
                        this.Subjects.OrderBy(i => i?.GetHashCode()).SequenceEqual(other.Subjects.OrderBy(i => i?.GetHashCode())) &&
                        this.Groups.OrderBy(i => i?.GetHashCode()).SequenceEqual(other.Groups.OrderBy(i => i?.GetHashCode())) &&
                        this.StudentFlows.OrderBy(i => i?.GetHashCode()).SequenceEqual(other.StudentFlows.OrderBy(i => i?.GetHashCode()));

                    if (result)
                    {
                        //Если отличается колличесво
                        if (this.LoadEntries.Count != other.LoadEntries.Count)
                        {
                            //Значит уже не равны
                            result = false;
                        }
                        else
                        {
                            bool foundEqualItem = false;

                            //Ищем аналог каждой записи из одной коллеции в другой
                            foreach (var thisItem in this.LoadEntries)
                            {
                                foreach (var otherItem in other.LoadEntries)
                                {
                                    //Если не нашли -- сравниваем следующую
                                    if (thisItem.ToString() != otherItem.ToString())
                                    {
                                        continue;
                                    }
                                    //Если нашли -- переходим к следующей записи верхнего списка
                                    else
                                    {
                                        foundEqualItem = true;
                                        break;
                                    }
                                }

                                //Если не нашли -- значит отличается
                                if (!foundEqualItem)
                                {
                                    result = false;
                                    break;
                                }
                            }
                        }
                    }

                    return result;
                }
            }
            else
            {
                return true;
            }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            return obj.GetType() == GetType() && Equals((DataModel)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = 0;
                hashCode = Title.GetHashCode();
                hashCode = hashCode ^ Teachers.Count ^ Subjects.Count ^ Groups.Count ^ StudentFlows.Count;
                return hashCode;
            }
        }

        #endregion
    }
}

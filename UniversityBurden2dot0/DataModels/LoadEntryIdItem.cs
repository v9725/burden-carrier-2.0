﻿
namespace UniversityBurden2dot0.DataModels
{
    /// <summary>
    /// Класс, прeдставляющий коллекцию идентификаторов строки таблицы Load_Entries.
    /// </summary>
    class LoadEntryIdItem
    {
        #region Fields

        /// <summary>
        /// Семестр: осенний(1) или весенний(2).
        /// </summary>
        public int SemesterId { get; set; }

        /// <summary>
        /// Предмет.
        /// </summary>
        public int SubjectId { get; set; }

        /// <summary>
        /// Преподаватель.
        /// </summary>
        public int TeacherId { get; set; }

        /// <summary>
        /// Вид учебной работы.
        /// </summary>
        public int StudyTypeId { get; set; }

        /// <summary>
        /// Поток студентов.
        /// </summary>
        public int StudentFlowId { get; set; }

        /// <summary>
        /// Количество часов.
        /// </summary>
        public int Hours { get; set; }

        #endregion

        #region Conturtor

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        /// <param name="semester_id">Семестр: осенний(1) или весенний(2).</param>
        /// <param name="subject_id">Предмет.</param>
        /// <param name="teacher_id">Преподаватель.</param>
        /// <param name="study_type_id">Вид учебной работы.</param>
        /// <param name="student_flow_id">Поток студентов.</param>
        /// <param name="hours">Количество часов.</param>
        public LoadEntryIdItem(
            int semester_id = -1,
            int subject_id = -1,
            int teacher_id = -1,
            int study_type_id = -1,
            int student_flow_id = -1,
            int hours = -1
        )
        {
            SemesterId = semester_id;
            SubjectId = subject_id;
            TeacherId = teacher_id;
            StudyTypeId = study_type_id;
            StudentFlowId = student_flow_id;
            Hours = hours;
        }

        #endregion
    }
}

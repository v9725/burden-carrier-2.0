﻿namespace UniversityBurden2dot0.Forms
{
    partial class AliasEditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AliasEditorForm));
            this.AddAliasButton = new System.Windows.Forms.Button();
            this.ButtonsPanel = new System.Windows.Forms.Panel();
            this.DeleteAliasButton = new System.Windows.Forms.Button();
            this.AliasKeyTextBox = new System.Windows.Forms.TextBox();
            this.AliasValueTextBox = new System.Windows.Forms.TextBox();
            this.AliasGridView = new System.Windows.Forms.DataGridView();
            this.ButtonsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AliasGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // AddAliasButton
            // 
            this.AddAliasButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddAliasButton.Location = new System.Drawing.Point(5, 5);
            this.AddAliasButton.Name = "AddAliasButton";
            this.AddAliasButton.Size = new System.Drawing.Size(242, 29);
            this.AddAliasButton.TabIndex = 2;
            this.AddAliasButton.Text = "Додати Псевдонім";
            this.AddAliasButton.UseVisualStyleBackColor = true;
            this.AddAliasButton.Click += new System.EventHandler(this.AddAliasButton_Click);
            // 
            // ButtonsPanel
            // 
            this.ButtonsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonsPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ButtonsPanel.Controls.Add(this.DeleteAliasButton);
            this.ButtonsPanel.Controls.Add(this.AddAliasButton);
            this.ButtonsPanel.Location = new System.Drawing.Point(12, 42);
            this.ButtonsPanel.Margin = new System.Windows.Forms.Padding(1);
            this.ButtonsPanel.Name = "ButtonsPanel";
            this.ButtonsPanel.Padding = new System.Windows.Forms.Padding(2);
            this.ButtonsPanel.Size = new System.Drawing.Size(501, 41);
            this.ButtonsPanel.TabIndex = 1;
            // 
            // DeleteAliasButton
            // 
            this.DeleteAliasButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DeleteAliasButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DeleteAliasButton.Location = new System.Drawing.Point(252, 5);
            this.DeleteAliasButton.Name = "DeleteAliasButton";
            this.DeleteAliasButton.Size = new System.Drawing.Size(242, 29);
            this.DeleteAliasButton.TabIndex = 3;
            this.DeleteAliasButton.Text = "Видалити Псевдонім";
            this.DeleteAliasButton.UseVisualStyleBackColor = true;
            this.DeleteAliasButton.Click += new System.EventHandler(this.DeleteAliasButton_Click);
            // 
            // AliasKeyTextBox
            // 
            this.AliasKeyTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AliasKeyTextBox.Location = new System.Drawing.Point(12, 12);
            this.AliasKeyTextBox.Name = "AliasKeyTextBox";
            this.AliasKeyTextBox.Size = new System.Drawing.Size(248, 24);
            this.AliasKeyTextBox.TabIndex = 0;
            this.AliasKeyTextBox.Text = "Sample Key";
            this.AliasKeyTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // AliasValueTextBox
            // 
            this.AliasValueTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AliasValueTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AliasValueTextBox.Location = new System.Drawing.Point(264, 12);
            this.AliasValueTextBox.Name = "AliasValueTextBox";
            this.AliasValueTextBox.Size = new System.Drawing.Size(248, 24);
            this.AliasValueTextBox.TabIndex = 1;
            this.AliasValueTextBox.Text = "Sample Value";
            this.AliasValueTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // AliasGridView
            // 
            this.AliasGridView.AllowUserToAddRows = false;
            this.AliasGridView.AllowUserToDeleteRows = false;
            this.AliasGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AliasGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.AliasGridView.Location = new System.Drawing.Point(12, 87);
            this.AliasGridView.Name = "AliasGridView";
            this.AliasGridView.ReadOnly = true;
            this.AliasGridView.RowTemplate.Height = 24;
            this.AliasGridView.Size = new System.Drawing.Size(501, 413);
            this.AliasGridView.TabIndex = 2;
            this.AliasGridView.SelectionChanged += new System.EventHandler(this.AliasGridView_SelectionChanged);
            // 
            // AliasEditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 510);
            this.Controls.Add(this.AliasGridView);
            this.Controls.Add(this.AliasValueTextBox);
            this.Controls.Add(this.AliasKeyTextBox);
            this.Controls.Add(this.ButtonsPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AliasEditorForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Налаштування псевдонимів";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AliasEditorForm_FormClosing);
            this.Load += new System.EventHandler(this.AliasEditorForm_Load);
            this.ButtonsPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AliasGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AddAliasButton;
        private System.Windows.Forms.Panel ButtonsPanel;
        private System.Windows.Forms.Button DeleteAliasButton;
        private System.Windows.Forms.TextBox AliasKeyTextBox;
        private System.Windows.Forms.TextBox AliasValueTextBox;
        private System.Windows.Forms.DataGridView AliasGridView;
    }
}
﻿using System;
using System.Xml;
using System.Linq;
using System.Xml.Linq;
using System.Collections.Generic;

namespace UniversityBurden2dot0.Utils
{
    /// <summary>
    /// Служебная структура.
    /// </summary>
    public struct AliasItem
    {
        public string SubjectKey { get; set; }
        public string AliasValue { get; set; }
    }

    /// <summary>
    /// Глобальный Словарь Псевдонимов для предметов.
    /// </summary>
    public static class AliasDictionary
    {
        /// <summary>
        /// Словарь псевдонимов.
        /// </summary>
        public static Dictionary<string, string> Aliases { get; private set; } = new Dictionary<string, string>();

        /// <summary>
        /// Возвращает псевдоним запрашиваемого предмета.
        /// </summary>
        /// <param name="subjectKey">Запрашиваемый предмет.</param>
        /// <returns>Псевдоним запрашиваемого предмета.</returns>
        public static string Alias(string subjectKey)
        {
            if (Aliases.ContainsKey(subjectKey))
                return Aliases[subjectKey];
            else
                return subjectKey;
        }

        #region Load-Store

        /// <summary>
        /// Создаёт файл с пустым словарём.
        /// </summary>
        public static void InitStorage()
        {
            if (!System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + "AliasesDictionary.xml"))
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.AppendChild(xmlDoc.CreateXmlDeclaration("1.0", null, null));
                xmlDoc.AppendChild(xmlDoc.CreateElement("AliasDictionary"));
                xmlDoc.Save("AliasesDictionary.xml");
                System.IO.File.SetAttributes(AppDomain.CurrentDomain.BaseDirectory + "AliasesDictionary.xml", System.IO.FileAttributes.Hidden);
            }
            else
                return;
        }

        /// <summary>
        /// Синхронизирует статическое хранилище и файл с внесением изменений.
        /// </summary>
        /// <param name="AliasesToAppend">Список псевдонимов, подлежащих добавлению.</param>
        /// <param name="AliasesToDelete">Список псевдонимов, подлежащих удалению.</param>
        /// <returns>спешность операции.</returns>
        public static bool SyncAliasesWithStorage(List<AliasItem> AliasesToAppend = null, List<AliasItem> AliasesToDelete = null)
        {
            //Вносим изменения
            if (AliasesToAppend != null)
            {
                //Загружаем документ.
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(AppDomain.CurrentDomain.BaseDirectory + "AliasesDictionary.xml");

                foreach (var item in AliasesToAppend)
                {
                    //Добавляем все новые псевдонимы.
                    AddAliasToDictionary(item.SubjectKey, item.AliasValue);

                    XmlNode itemNode = xmlDoc.CreateElement("item");

                    XmlNode keyNode = xmlDoc.CreateElement("subject");
                    keyNode.InnerText = item.SubjectKey;
                    itemNode.AppendChild(keyNode);

                    XmlNode valueNode = xmlDoc.CreateElement("alias");
                    valueNode.InnerText = item.AliasValue;
                    itemNode.AppendChild(valueNode);

                    xmlDoc.DocumentElement.AppendChild(itemNode);
                }

                //Сохраняем.
                try
                {
                    System.IO.File.Delete(AppDomain.CurrentDomain.BaseDirectory + "AliasesDictionary.xml");
                    xmlDoc.Save(AppDomain.CurrentDomain.BaseDirectory + "AliasesDictionary.xml");
                    System.IO.File.SetAttributes(AppDomain.CurrentDomain.BaseDirectory + "AliasesDictionary.xml", System.IO.FileAttributes.Hidden);
                    return true;
                }
                catch (Exception exc)
                {
                    System.Windows.Forms.MessageBox.Show(exc.Message + "\n============================\n" + exc.StackTrace);
                    //Логирование
                    return false;
                }
            }
            if (AliasesToDelete != null)
            {
                XDocument xmlDoc = XDocument.Load(AppDomain.CurrentDomain.BaseDirectory + "AliasesDictionary.xml");

                foreach (var item in AliasesToDelete)
                {
                    //Удаляем все псевдонимы, подлежащие удалению.
                    RemoveAliasFromDictionary(item.SubjectKey);

                    var nodeToDelete = (from node in xmlDoc.Descendants("item")
                                        where node.Element("subject").Value == item.SubjectKey
                                        select node).FirstOrDefault();
                    nodeToDelete.Remove();

                    //Сохраняем.
                    try
                    {
                        System.IO.File.Delete(AppDomain.CurrentDomain.BaseDirectory + "AliasesDictionary.xml");
                        xmlDoc.Save(AppDomain.CurrentDomain.BaseDirectory + "AliasesDictionary.xml");
                        System.IO.File.SetAttributes(AppDomain.CurrentDomain.BaseDirectory + "AliasesDictionary.xml", System.IO.FileAttributes.Hidden);
                    }
                    catch (Exception exc)
                    {
                        System.Windows.Forms.MessageBox.Show(exc.Message + "\n============================\n" + exc.StackTrace);
                        //Логирование
                        return false;
                    }
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Загружает сохранённые на диске псевдонимы в программу.
        /// </summary>
        /// <returns>Успешность операции.</returns>
        public static bool LoadAliasesFromStorage()
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(AppDomain.CurrentDomain.BaseDirectory + "AliasesDictionary.xml");
                {
                    foreach (XmlNode node in xmlDoc.DocumentElement.SelectNodes("/AliasDictionary/item"))
                    {
                        if (!Aliases.ContainsKey(node.FirstChild.InnerText))
                        { Aliases.Add(node.FirstChild.InnerText, node.LastChild.InnerText); }
                    }
                }

                return true;
            }
            catch (Exception exc)
            {
                System.Windows.Forms.MessageBox.Show(exc.Message + "\n============================\n" + exc.StackTrace);
                //логирование
                return false;
            }
        }

        /// <summary>
        /// Сохраняет псевдонимы на диск из статического хранилища.
        /// </summary>
        /// <returns>Успешность операции.</returns>
        public static bool StoreAliasesToStorage()
        {
            XmlDocument xmlDoc = new XmlDocument();
            {
                XmlNode declaration = xmlDoc.CreateXmlDeclaration("1.0", null, null);
                xmlDoc.AppendChild(declaration);

                XmlNode rootNode = xmlDoc.CreateElement("AliasDictionary");
                {
                    foreach (var item in Aliases)
                    {
                        XmlNode itemNode = xmlDoc.CreateElement("item");

                        XmlNode keyNode = xmlDoc.CreateElement("subject");
                        keyNode.InnerText = item.Key;
                        itemNode.AppendChild(keyNode);

                        XmlNode valueNode = xmlDoc.CreateElement("alias");
                        valueNode.InnerText = item.Value;
                        itemNode.AppendChild(valueNode);

                        rootNode.AppendChild(itemNode);
                    }
                }
                xmlDoc.AppendChild(rootNode);
            }

            try
            {
                xmlDoc.Save(AppDomain.CurrentDomain.BaseDirectory + "AliasesDictionary.xml");
                System.IO.File.SetAttributes(AppDomain.CurrentDomain.BaseDirectory + "AliasesDictionary.xml", System.IO.FileAttributes.Hidden);
                return true;
            }
            catch (Exception exc)
            {
                System.Windows.Forms.MessageBox.Show(exc.Message + "\n============================\n" + exc.StackTrace);
                return false;
            }
        }

        #endregion

        #region Add-Remove

        /// <summary>
        /// Добавляет новый псевдоним в словарь.
        /// </summary>
        /// <param name="subjectKey">Название предмета.</param>
        /// <param name="aliasValue">Псевдоним для предмета.</param>
        /// <returns>Успешность операции.</returns>
        public static bool AddAliasToDictionary(string subjectKey, string aliasValue)
        {
            if (!Aliases.ContainsKey(subjectKey))
            {
                Aliases.Add(subjectKey, aliasValue);
                return true;
            }
            else
            { return false; }
        }

        /// <summary>
        /// Удаляет псевдоним из словаря.
        /// </summary>
        /// <param name="subjectKey">Предмет, псевдоним когорого требуется удалить.</param>
        /// <returns>Успешность операции.</returns>
        public static bool RemoveAliasFromDictionary(string subjectKey)
        {
            if (Aliases.ContainsKey(subjectKey))
            {
                Aliases.Remove(subjectKey);
                return true;
            }
            else
            { return false; }
        }

        #endregion
    }
}

﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;

using UniversityBurden2dot0.Utils;

namespace UniversityBurden2dot0.Forms
{
    /// <summary>
    /// Форма для редактирования псевдонимов предметов.
    /// </summary>
    public partial class AliasEditorForm : Form
    {
        #region Fields

        /// <summary>
        /// Буфер для псевдонимов.
        /// </summary>
        List<AliasItem> Aliases;

        /// <summary>
        /// Перечень псевдонимов, подлежащих удалению.
        /// </summary>
        List<AliasItem> AliasesToDelete;

        /// <summary>
        /// Перечень псевдонимов подлежащих записи в файл.
        /// </summary>
        List<AliasItem> AliasesToStore;

        #endregion

        #region Constructor

        /// <summary>
        /// Конструктор формы.
        /// </summary>
        public AliasEditorForm()
        {
            InitializeComponent();

            //Иницииализация коллекций.
            Aliases = new List<AliasItem>();
            AliasesToStore = new List<AliasItem>();
            AliasesToDelete = new List<AliasItem>();

            //Импортируем Псевдонимы из статического хранилища.
            foreach (var item in AliasDictionary.Aliases)
            { Aliases.Add(new AliasItem { SubjectKey = item.Key, AliasValue = item.Value }); }

            this.Icon = Properties.Resources.settings_icon;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Обработчик открытия формы.
        /// </summary>
        private void AliasEditorForm_Load(object sender, EventArgs e)
        {
            //Очистка полей ввода.
            AliasKeyTextBox.Clear();
            AliasValueTextBox.Clear();

            //Отключение кнопки удаления.
            DeleteAliasButton.Enabled = false;

            //Подвязка и настрйока грида.
            AliasGridView.DataSource = Aliases;

            AliasGridView.Columns[0].HeaderText = "Дисциплина";
            AliasGridView.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            AliasGridView.Columns[1].HeaderText = "Псевдоним";
            AliasGridView.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;


            //Фокусировка.
            AliasGridView.Refresh();
            AliasGridView.Focus();
        }

        /// <summary>
        /// Обработчик закрытия формы.
        /// </summary>
        private void AliasEditorForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Синхронизация изменений со статическим хранилищем.
            if (!SyncAliases())
            {
                MessageBox.Show("Something went wrong during aliases saving!", "ERRROR!");
                //Заменить на логирование.
            }
        }

        /// <summary>
        /// Обработка изменения выделения таблицы псевдонимов.
        /// </summary>
        private void AliasGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (AliasGridView.SelectedRows.Count > 0)
            { DeleteAliasButton.Enabled = true; }
            else
            { DeleteAliasButton.Enabled = false; }
        }

        #region Buttons

        /// <summary>
        /// Обрабатывает попытку добавления нового псевдонима.
        /// </summary>
        private void AddAliasButton_Click(object sender, EventArgs e)
        {
            if (Aliases.Contains(new AliasItem { SubjectKey = AliasKeyTextBox.Text, AliasValue = AliasValueTextBox.Text }))
            {
                MessageBox.Show(
                    "Alias for \"" + AliasValueTextBox.Text + "\" already exists!",
                    "Error!",
                    MessageBoxButtons.OK);
            }
            else
            {
                //Добавляем и обновляем представление

                //Сбрассываем выдиление
                AliasGridView.ClearSelection();

                //Добавляем и перепривязываем
                AliasGridView.DataSource = null;
                Aliases.Add(new AliasItem { SubjectKey = AliasKeyTextBox.Text, AliasValue = AliasValueTextBox.Text });
                AliasGridView.DataSource = Aliases;

                //настраиваем
                AliasGridView.Columns[0].HeaderText = "Дисциплина";
                AliasGridView.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                AliasGridView.Columns[1].HeaderText = "Псевдоним";
                AliasGridView.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;


                //Заносим в список на синхронизацию если нужно
                if (!AliasDictionary.Aliases.ContainsKey(AliasKeyTextBox.Text))
                    AliasesToStore.Add(new AliasItem { SubjectKey = AliasKeyTextBox.Text, AliasValue = AliasValueTextBox.Text });

                //Очищаем поля ввода и фокусируем.
                AliasKeyTextBox.Clear();
                AliasValueTextBox.Clear();
                AliasGridView.Refresh();
            }
        }

        /// <summary>
        /// Обрадатывает попытку удалить псевдоним.
        /// </summary>
        private void DeleteAliasButton_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in AliasGridView.SelectedRows)
            {
                //Если есть в статическом хранилище -- добавляем в список на удаление.
                if (AliasDictionary.Aliases.ContainsKey(row.Cells[0].Value.ToString()))
                {
                    AliasesToDelete.Add(
                        new AliasItem
                        {
                            SubjectKey = row.Cells[0].Value.ToString(),
                            AliasValue = row.Cells[1].Value.ToString()
                        });
                }

                //удаляем из буфера и обновляем представление
                Aliases.Remove(
                    new AliasItem
                    {
                        SubjectKey = row.Cells[0].Value.ToString(),
                        AliasValue = row.Cells[1].Value.ToString()
                    });
            }
            AliasGridView.ClearSelection();
            AliasGridView.DataSource = null;
            AliasGridView.DataSource = Aliases;

            AliasGridView.Columns[0].HeaderText = "Дисциплина";
            AliasGridView.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            AliasGridView.Columns[1].HeaderText = "Псевдоним";
            AliasGridView.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            AliasGridView.Refresh();
        }

        #endregion

        #endregion

        #region Logic

        /// <summary>
        /// Сохраняет все изменения в статическое хранилище и в файл.
        /// </summary>
        /// <returns>Успешность операции.</returns>
        private bool SyncAliases()
        {
            if (AliasesToStore.Count > 0 || AliasesToDelete.Count > 0)
            {
                return AliasDictionary.SyncAliasesWithStorage(
                    (AliasesToStore.Count > 0) ? AliasesToStore : null,
                    (AliasesToDelete.Count > 0) ? AliasesToDelete : null
                    );
            }
            else
                return true;
        }

        #endregion
    }
}

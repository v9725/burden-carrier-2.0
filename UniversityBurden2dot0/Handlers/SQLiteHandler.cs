﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;

using UniversityBurden2dot0.DataModels;

namespace UniversityBurden2dot0.Handlers
{
    /// <summary>
    /// Класс-слой работы с данными, ориентированный на СУБД SQLite.
    /// </summary>
    public class SQLiteHandler
    {
        #region Fields and Properties

        #region Private

        /// <summary>
        /// Коннектор к БД.
        /// </summary>
        private SQLiteConnection _connection;

        #endregion

        #endregion

        #region Events and Delegates

        /// <summary>
        /// Базовый делегат.
        /// </summary>
        public delegate void GeneralEventHandler(object sender, EventArgs eventArgs);

        /// <summary>
        /// Делегат событий отмены.
        /// </summary>
        public delegate void CancelEventHandler(object sender, System.ComponentModel.CancelEventArgs Eventargs);

        #endregion

        #region Constructor

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        public SQLiteHandler()
        { }

        #endregion

        #region initCycle

        /// <summary>
        /// Проводит проверку БД.
        /// </summary>
        public bool InitCheck()
        {
            //Проверяем есть ли путь в настройках.
            if (
                string.IsNullOrEmpty(Properties.Settings.Default.DBFilePath) || 
                string.IsNullOrWhiteSpace(Properties.Settings.Default.DBFilePath)
            )
            {
                //Если пути нет
                return InitDialog();
            }
            //Если путь есть
            else
            {
                //проверяем есть ли файл по заявленному пути
                if (System.IO.File.Exists(Properties.Settings.Default.DBFilePath))
                {
                    //Если файл есть

                    //Подключаемся
                    string connectionString = string.Format("data source={0};New=True;UseUTF16Encoding=True", Properties.Settings.Default.DBFilePath);
                    _connection = new SQLiteConnection(connectionString);

                    //Проверяем нужной ли стурктуры БД
                    if (HasProperStructure())
                    {
                        //сообщаем котроллеру об успешном завершении
                        return true;
                    }
                    else
                    {
                        //Отсоединяемся и ругаемся
                        DisconnectFromDB();
                        throw new Exception("Database doesn't have proper structure!");
                    }
                }
                //Если файла нет
                else
                {
                    return InitDialog();
                }
            }
        }

        /// <summary>
        /// Порождает и обробатывает диалог создания/выбора ДБ.
        /// </summary>
        private bool InitDialog()
        {
            //Предлагаем юзеру два варианта: создать чистую или выбрать кастомный путь.

            //Создаём экземпляр формы
            Forms.DBDialogForm dbDialogForm = new Forms.DBDialogForm();

            //Если он вовсе закрыл диалог
            if (dbDialogForm.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
            {
                //Отменяем всё и закрываем приложение
                return false;
            }
            //Если он выбрал один и вариантов
            else
            {
                //Определяем что он выбрал

                //Если создать новую
                if (dbDialogForm.CreateNew)
                {
                    //создаём файл и скрываем его
                    SQLiteConnection.CreateFile("storage.db");
                    System.IO.File.SetAttributes(AppDomain.CurrentDomain.BaseDirectory + "storage.db", System.IO.FileAttributes.Hidden);

                    //подключаемся
                    string connectionString = string.Format("data source={0};New=True;UseUTF16Encoding=True", AppDomain.CurrentDomain.BaseDirectory + "storage.db");
                    _connection = new SQLiteConnection(connectionString);

                    //создаём структуру
                    CreateTables();
                    PreFillTables(true);

                    //заносим в настойки и сохраняем
                    Properties.Settings.Default.DBFilePath = AppDomain.CurrentDomain.BaseDirectory + "storage.db";
                    Properties.Settings.Default.Save();

                    //сообщаем котроллеру об успешном завершении
                    return true;
                }
                //Если выбрать кастомный путь
                else
                {
                    //Подключаемся
                    string connectionString = string.Format("data source={0};New=True;UseUTF16Encoding=True", dbDialogForm.CustomDBPath);
                    _connection = new SQLiteConnection(connectionString);

                    //и проверяем подходит ли нам БД по структуре
                    if (HasProperStructure())
                    {
                        //Если да -- заносим в настройки валидный путь
                        Properties.Settings.Default.DBFilePath = dbDialogForm.CustomDBPath;
                        Properties.Settings.Default.Save();

                        //и сообщаем контроллеру об успешном завершении
                        return true;
                    }
                    else
                    {
                        //Если нет -- отсоединяемся и ругаемся
                        DisconnectFromDB();
                        throw new Exception("Database doesn't have proper structure!");
                    }
                }
            }
        }

        #endregion

        #region Logic

        #region DB: (Dis)Connection

        /// <summary>
        /// Обнуляет связь с БД
        /// </summary>
        public void DisconnectFromDB()
        {
            _connection = null;
        }

        #endregion

        #region DB: Creation

        /// <summary>
        /// Создаёт таблицы базы данных.
        /// </summary>
        private void CreateTables()
        {
            #region SQL
            //Запросы для создания необходимых таблиц
            string sqlCreateFilesTable          = "CREATE TABLE IF NOT EXISTS FILES (id INTEGER NOT NULL DEFAULT 1 PRIMARY KEY AUTOINCREMENT UNIQUE, filename TEXT NOT NULL, date TEXT NOT NULL, version INTEGER NOT NULL)";
            string sqlCreateGroupsTable         = "CREATE TABLE IF NOT EXISTS GROUPS (id INTEGER NOT NULL DEFAULT 1 PRIMARY KEY AUTOINCREMENT UNIQUE, name TEXT NOT NULL UNIQUE)";
            string sqlCreateSemestersTable      = "CREATE TABLE IF NOT EXISTS SEMESTERS (id INTEGER NOT NULL DEFAULT 1 PRIMARY KEY AUTOINCREMENT UNIQUE, name TEXT NOT NULL UNIQUE)";
            string sqlCreateStudentFlowsTable   = "CREATE TABLE IF NOT EXISTS STUDENT_FLOWS (id INTEGER NOT NULL DEFAULT 1 PRIMARY KEY AUTOINCREMENT UNIQUE, name TEXT NOT NULL UNIQUE)";
            string sqlCreateStudentGrFlTable    = "CREATE TABLE IF NOT EXISTS STUDENT_GR_FL (FID_group INTEGER NOT NULL, FID_flow INTEGER NOT NULL, FOREIGN KEY(FID_group) REFERENCES GROUPS (id), FOREIGN KEY(FID_flow) REFERENCES STUDENT_FLOWS(id))";
            string sqlCreateStudiesTypesTable   = "CREATE TABLE IF NOT EXISTS STUDY_TYPES (id INTEGER NOT NULL DEFAULT 1 PRIMARY KEY AUTOINCREMENT UNIQUE, name TEXT NOT NULL UNIQUE)";
            string sqlCreateSubjectsTable       = "CREATE TABLE IF NOT EXISTS SUBJECTS (id INTEGER NOT NULL DEFAULT 1 PRIMARY KEY AUTOINCREMENT UNIQUE, name TEXT NOT NULL UNIQUE)";
            string sqlCreateTeachersTable       = "CREATE TABLE IF NOT EXISTS TEACHERS (id INTEGER NOT NULL DEFAULT 1 PRIMARY KEY AUTOINCREMENT UNIQUE, name TEXT NOT NULL UNIQUE)";

            string sqlCreateLoadMainTable       = "CREATE TABLE IF NOT EXISTS LOAD_ENTRIES (id INTEGER NOT NULL DEFAULT 1 PRIMARY KEY AUTOINCREMENT UNIQUE, FID_file_id INTEGER NOT NULL, FID_semester INTEGER NOT NULL, FID_teacher INTEGER NOT NULL, FID_subject INTEGER NOT NULL, FID_student_flow INTEGER NOT NULL, FID_study_type INTEGER NOT NULL, hours INTEGER NOT NULL, FOREIGN KEY(FID_student_flow) REFERENCES STUDENT_FLOWS(id), FOREIGN KEY(FID_semester) REFERENCES SEMESTERS(id), FOREIGN KEY(FID_subject) REFERENCES SUBJECTS(id), FOREIGN KEY(FID_teacher) REFERENCES TEACHERS(id), FOREIGN KEY(FID_study_type) REFERENCES STUDIES_TYPES(id), FOREIGN KEY(FID_file_id) REFERENCES FILES(id))";
            #endregion

            //Открываем соединение и выполняем запросы
            try
            {
                _connection.Open();

                #region SQLiteCommand
                //Комманды на основе запросов
                SQLiteCommand cmdCreateFilesTable           = new SQLiteCommand(sqlCreateFilesTable, _connection);
                SQLiteCommand cmdCreateGroupsTable          = new SQLiteCommand(sqlCreateGroupsTable, _connection);
                SQLiteCommand cmdCreateSemestersTable       = new SQLiteCommand(sqlCreateSemestersTable, _connection);
                SQLiteCommand cmdCreateStudentFlowsTable    = new SQLiteCommand(sqlCreateStudentFlowsTable, _connection);
                SQLiteCommand cmdCreateStudentGrFlTable     = new SQLiteCommand(sqlCreateStudentGrFlTable, _connection);
                SQLiteCommand cmdCreateStudiesTypesTable    = new SQLiteCommand(sqlCreateStudiesTypesTable, _connection);
                SQLiteCommand cmdCreateSubjectsTable        = new SQLiteCommand(sqlCreateSubjectsTable, _connection);
                SQLiteCommand cmdCreateTeacherstable        = new SQLiteCommand(sqlCreateTeachersTable, _connection);

                SQLiteCommand cmdCreateLoadMainTable        = new SQLiteCommand(sqlCreateLoadMainTable, _connection);
                #endregion

                #region Execution
                cmdCreateFilesTable.ExecuteNonQuery();
                cmdCreateGroupsTable.ExecuteNonQuery();
                cmdCreateSemestersTable.ExecuteNonQuery();
                cmdCreateStudentFlowsTable.ExecuteNonQuery();
                cmdCreateStudentGrFlTable.ExecuteNonQuery();
                cmdCreateStudiesTypesTable.ExecuteNonQuery();
                cmdCreateSubjectsTable.ExecuteNonQuery();
                cmdCreateTeacherstable.ExecuteNonQuery();

                cmdCreateLoadMainTable.ExecuteNonQuery();
                #endregion

            }
            catch (Exception exc)
            {
                System.Windows.Forms.MessageBox.Show(exc.Message + "\n=========================\n" + exc.StackTrace);
            }
            finally
            {
                _connection.Close();
            }
        }

        /// <summary>
        /// Заполняет некоторые таблицы служебными данными.
        /// </summary>
        /// <param name="close">Флаг, указывающий, закрывать ли за собой соединение (на случай вызова из функции с открытым соединением).</param>
        private void PreFillTables(bool close)
        {
            #region SQL
            //Семестры
            string sqlInsertSemesters = "INSERT INTO SEMESTERS(name) VALUES(@name)";
            
            //Типы работ
            string sqlInsertStudyType = "INSERT INTO STUDY_TYPES(name) VALUES(@name)";
            #endregion

            try
            {
                if(_connection.State != System.Data.ConnectionState.Open) { _connection.Open(); }

                #region SQLiteCommand
                //Семестры
                SQLiteCommand cmdInsertFirstSemester    = new SQLiteCommand(sqlInsertSemesters, _connection);
                SQLiteCommand cmdInsertSecondSemester   = new SQLiteCommand(sqlInsertSemesters, _connection);

                //Типы работ
                SQLiteCommand cmdInsertLectures             = new SQLiteCommand(sqlInsertStudyType, _connection);
                SQLiteCommand cmdInsertPracticeAssignments  = new SQLiteCommand(sqlInsertStudyType, _connection);
                SQLiteCommand cmdInsertLabAssignments       = new SQLiteCommand(sqlInsertStudyType, _connection);
                SQLiteCommand cmdInsertConsulting           = new SQLiteCommand(sqlInsertStudyType, _connection);
                SQLiteCommand cmdInsertKP_KR                = new SQLiteCommand(sqlInsertStudyType, _connection);
                SQLiteCommand cmdInsertKR_RGZ               = new SQLiteCommand(sqlInsertStudyType, _connection);
                SQLiteCommand cmdInsertSemesterCheck        = new SQLiteCommand(sqlInsertStudyType, _connection);
                SQLiteCommand cmdInsertDiplomaWork          = new SQLiteCommand(sqlInsertStudyType, _connection);
                SQLiteCommand cmdInsertExamsWork            = new SQLiteCommand(sqlInsertStudyType, _connection);
                SQLiteCommand cmdInsertTraineeship          = new SQLiteCommand(sqlInsertStudyType, _connection); //аспирантура
                SQLiteCommand cmdInsertMagistracy           = new SQLiteCommand(sqlInsertStudyType, _connection);
                SQLiteCommand cmdInsertSummerPractice       = new SQLiteCommand(sqlInsertStudyType, _connection);
                #endregion

                #region Params
                //Семестры
                cmdInsertFirstSemester.Parameters.AddWithValue("@name", "Осінній семестр");
                cmdInsertSecondSemester.Parameters.AddWithValue("@name", "Весняний семестр");

                //Типы работ
                cmdInsertLectures.Parameters.AddWithValue("@name", "Лекції");
                cmdInsertPracticeAssignments.Parameters.AddWithValue("@name", "Практичні Роботи");
                cmdInsertLabAssignments.Parameters.AddWithValue("@name", "Лабораторні Роботи");
                cmdInsertConsulting.Parameters.AddWithValue("@name", "Консультації");
                cmdInsertKP_KR. Parameters.AddWithValue("@name", "КП/КР");
                cmdInsertKR_RGZ.Parameters.AddWithValue("@name", "Контр. роботи і РГЗ");
                cmdInsertSemesterCheck.Parameters.AddWithValue("@name", "Семестровий контроль");
                cmdInsertDiplomaWork.Parameters.AddWithValue("@name", "Дипломування");
                cmdInsertExamsWork.Parameters.AddWithValue("@name", "ЕК");
                cmdInsertTraineeship.Parameters.AddWithValue("@name", "Аспірантура");
                cmdInsertMagistracy.Parameters.AddWithValue("@name", "Магістратура");
                cmdInsertSummerPractice.Parameters.AddWithValue("@name", "Практика");
                #endregion

                #region Execution
                //Семестры
                cmdInsertFirstSemester.ExecuteNonQuery();
                cmdInsertSecondSemester.ExecuteNonQuery();

                //Типы работ
                cmdInsertLectures.ExecuteNonQuery();
                cmdInsertPracticeAssignments.ExecuteNonQuery();
                cmdInsertLabAssignments.ExecuteNonQuery();
                cmdInsertConsulting.ExecuteNonQuery();
                cmdInsertKP_KR.ExecuteNonQuery();
                cmdInsertKR_RGZ.ExecuteNonQuery();
                cmdInsertSemesterCheck.ExecuteNonQuery();
                cmdInsertDiplomaWork.ExecuteNonQuery();
                cmdInsertExamsWork.ExecuteNonQuery();
                cmdInsertTraineeship.ExecuteNonQuery();
                cmdInsertMagistracy.ExecuteNonQuery();
                cmdInsertSummerPractice.ExecuteNonQuery();
                #endregion

            }
            catch (Exception exc)
            {
                System.Windows.Forms.MessageBox.Show(exc.Message + "\n=========================\n" + exc.StackTrace);
            }
            finally
            {
                if (close) { _connection.Close(); }
            }
        }

        #endregion

        #region DB: File(s) Importing

        /// <summary>
        /// Добавляет в БД модель из единичного файла.
        /// </summary>
        /// <param name="model">Ссылка на импортируемую модель.</param>
        /// <param name="close">Флаг, указывающий, закрывать ли за собой соединение (на случай вызова из функции с открытым соединением).</param>
        /// <returns>Успешность выполнения операции.</returns>
        public bool InsertDataModel(DataModel model, out int insertedFileId, bool close = true)
        {
            bool operationResult = false;

            #region Metadata
            int fileId = -1;

            Dictionary<string, int> modelTeachersIds = new Dictionary<string, int>(); //name, id
            Dictionary<string, int> modelSubjectsIds = new Dictionary<string, int>(); //name, id
            Dictionary<string, int> modelGroupsIds = new Dictionary<string, int>(); //name, id
            Dictionary<string, int> modelStFlowsIds = new Dictionary<string, int>(); //name, id

            Dictionary<int, List<int>> modelStGrFl = new Dictionary<int, List<int>>(); //flow id, [groups ids]
            #endregion

            #region SQL
            string sqlInsertFile = "INSERT OR ABORT INTO FILES(filename, date, version) VALUES(@filename, @date, @version)";
            string sqlGetFileId = "SELECT id FROM FILES WHERE date = @date AND filename = @filename AND version = @version";

            string sqlInsertTeachers = "INSERT OR IGNORE INTO TEACHERS(name) VALUES(@name)";
            string sqlInsertSubjects = "INSERT OR IGNORE INTO SUBJECTS(name) VALUES(@name)";
            string sqlInsertGroups = "INSERT OR IGNORE INTO GROUPS(name) VALUES(@name)";
            string sqlInsertStudentFlows = "INSERT OR IGNORE INTO STUDENT_FLOWS(name) VALUES(@name)";

            string sqlInsertGrFl = "INSERT OR IGNORE INTO STUDENT_GR_FL VALUES(@FID_group, @FID_flow)";
            string sqlInsertLoadEntry = "INSERT INTO LOAD_ENTRIES(FID_file_id, FID_semester, FID_teacher, FID_subject, FID_student_flow, FID_study_type, hours) VALUES(@FID_file_id, @FID_semester, @FID_teacher, @FID_subject, @FID_student_flow, @FID_study_type, @hours)";
            #endregion

            #region Execution
            try
            {
                if (_connection != null && _connection.State != System.Data.ConnectionState.Open) { _connection.Open(); }

                #region File
                SQLiteCommand cmdInsertFile = new SQLiteCommand(sqlInsertFile, _connection);
                cmdInsertFile.Parameters.AddWithValue("@filename", model.Title.FileName);
                cmdInsertFile.Parameters.AddWithValue("@date", model.Title.Date);
                cmdInsertFile.Parameters.AddWithValue("@version", model.Title.Version);
                operationResult = (cmdInsertFile.ExecuteNonQuery() >= 1) ? true : false;

                SQLiteCommand cmdGetFileId = new SQLiteCommand(sqlGetFileId, _connection);
                cmdGetFileId.Parameters.AddWithValue("@date", model.Title.Date);
                cmdGetFileId.Parameters.AddWithValue("@filename", model.Title.FileName);
                cmdGetFileId.Parameters.AddWithValue("@version", model.Title.Version);
                using (SQLiteDataReader dataReader = cmdGetFileId.ExecuteReader())
                {
                    while (dataReader.Read())
                    { fileId = dataReader.GetInt32(0); }
                }
                #endregion

                int I = 0;

                #region Teachers
                if ((I = GetLastId("TEACHERS")) == -1) I = 0;
                for (int i = 0; i < model.Teachers.Count; i++)
                {
                    SQLiteCommand cmdInsertTeacher = new SQLiteCommand(sqlInsertTeachers, _connection);
                    cmdInsertTeacher.Parameters.AddWithValue("@name", model.Teachers[i]);

                    int res = cmdInsertTeacher.ExecuteNonQuery();
                    //if (cmdInsertTeacher.ExecuteNonQuery() == 1)
                    // {
                    modelTeachersIds.Add(model.Teachers[i], GetIDByName("TEACHERS", model.Teachers[i]));
                    //}
                   // else
                   // { modelTeachersIds.Add(model.Teachers[i], GetIDByName("TEACHERS", model.Teachers[i])); }
                }
                #endregion

                #region Subjects
                if ((I = GetLastId("SUBJECTS")) == -1) I = 0;
                for (int i = 0; i < model.Subjects.Count; i++)
                {
                    SQLiteCommand cmdInsertSubject = new SQLiteCommand(sqlInsertSubjects, _connection);
                    cmdInsertSubject.Parameters.AddWithValue("@name", model.Subjects[i]);

                    int res = cmdInsertSubject.ExecuteNonQuery();
                    //if (cmdInsertSubject.ExecuteNonQuery() == 1)
                    //{ modelSubjectsIds.Add(model.Subjects[i], ++I); }
                    //else
                    //{
                        modelSubjectsIds.Add(model.Subjects[i], GetIDByName("SUBJECTS", model.Subjects[i]));
                    //}
                }
                #endregion

                #region Groups
                if ((I = GetLastId("GROUPS")) == -1) I = 0;
                for (int i = 0; i < model.Groups.Count; i++)
                {
                    SQLiteCommand cmdInsertGroups = new SQLiteCommand(sqlInsertGroups, _connection);
                    cmdInsertGroups.Parameters.AddWithValue("@name", model.Groups[i]);

                    int res = cmdInsertGroups.ExecuteNonQuery();
                    //if (res == 1)
                    //{
                    modelGroupsIds.Add(model.Groups[i], GetIDByName("GROUPS", model.Groups[i]));
                    //}
                   // else
                   // { modelGroupsIds.Add(model.Groups[i], GetIDByName("GROUPS", model.Groups[i])); }
                }
                #endregion

                #region Student Flows
                if ((I = GetLastId("STUDENT_FLOWS")) == -1) I = 0;
                for (int i = 0; i < model.StudentFlows.Count; i++)
                {
                    SQLiteCommand cmdInsertFlows = new SQLiteCommand(sqlInsertStudentFlows, _connection);
                    cmdInsertFlows.Parameters.AddWithValue("@name", model.StudentFlows[i]);


                    int res = cmdInsertFlows.ExecuteNonQuery();
                    modelStFlowsIds.Add(model.StudentFlows[i], GetIDByName("STUDENT_FLOWS", model.StudentFlows[i]));
                    // if (cmdInsertFlows.ExecuteNonQuery() == 1)
                    //{ modelStFlowsIds.Add(model.StudentFlows[i], ++I); }
                    //else
                    //{ modelStFlowsIds.Add(model.StudentFlows[i], GetIDByName("STUDENT_FLOWS", model.StudentFlows[i])); }
                }
                #endregion

                #region Flows <--> Groups Binding
                foreach (var flow in modelStFlowsIds)
                {
                    string[] groups = DataMapper.FlowToGroups(flow.Key);

                    foreach (var group in groups)
                    {
                        SQLiteCommand cmdInsertStGrFl = new SQLiteCommand(sqlInsertGrFl, _connection);
                        cmdInsertStGrFl.Parameters.AddWithValue("@FID_group", modelGroupsIds[group]);
                        cmdInsertStGrFl.Parameters.AddWithValue("@FID_flow", flow.Value);

                        operationResult &= (cmdInsertStGrFl.ExecuteNonQuery() == 1) ? true : false;
                    }
                }
                #endregion

                #region Load Entries
                foreach (var loadEntry in model.LoadEntries)
                {
                    SQLiteCommand cmdInsertLoadEntry = new SQLiteCommand(sqlInsertLoadEntry, _connection);

                    cmdInsertLoadEntry.Parameters.AddWithValue("@FID_file_id", fileId);
                    cmdInsertLoadEntry.Parameters.AddWithValue("@FID_semester", loadEntry.Semester);
                    cmdInsertLoadEntry.Parameters.AddWithValue("@FID_teacher", modelTeachersIds[loadEntry.Teacher]);
                    cmdInsertLoadEntry.Parameters.AddWithValue("@FID_subject", modelSubjectsIds[loadEntry.Subject]);
                    cmdInsertLoadEntry.Parameters.AddWithValue("@FID_student_flow", modelStFlowsIds[loadEntry.StudentFlow]);
                    cmdInsertLoadEntry.Parameters.AddWithValue("@FID_study_type", GetIDByName("STUDY_TYPES", loadEntry.StudyType));
                    cmdInsertLoadEntry.Parameters.AddWithValue("@hours", loadEntry.Hours);

                    operationResult &= (cmdInsertLoadEntry.ExecuteNonQuery() == 1) ? true : false;
                }
                #endregion
            }
            catch (Exception exc)
            {
                //Логирование
                System.Windows.Forms.MessageBox.Show(exc.Message + "\n=========================\n" + exc.StackTrace);
                insertedFileId = -1;
                return false;
            }
            finally
            { if (close) { _connection.Close(); } }
            #endregion

            insertedFileId = fileId;
            return operationResult;
        }

        #endregion

        #region DB: Checkers

        /// <summary>
        /// Проверяет, содержит ли база данных необходимую структуру.
        /// </summary>
        /// <returns>Булевый результат проверки.</returns>
        public bool HasProperStructure()
        {
            if (_connection != null)
            {
                bool res = false;

                try
                {
                    //Открываем соединение
                    _connection.Open();

                    //Получаем названия всех таблиц из БД.
                    string sqlGetTables = "SELECT name FROM sqlite_master WHERE type = 'table'";
                    SQLiteCommand cmdGetTables = new SQLiteCommand(sqlGetTables, _connection);

                    //Згружаем все названия с список.
                    List<string> tables = new List<string>();
                    using (SQLiteDataReader dataReader = cmdGetTables.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            tables.Add(dataReader.GetString(0));
                        }
                    }

                    //Проверяем чтобы присутствовали все таблицы.
                    res = tables.Contains("FILES") 
                       && tables.Contains("GROUPS")
                       && tables.Contains("SEMESTERS")
                       && tables.Contains("STUDENT_FLOWS")
                       && tables.Contains("STUDENT_GR_FL")
                       && tables.Contains("STUDY_TYPES")
                       && tables.Contains("SUBJECTS")
                       && tables.Contains("TEACHERS")
                       && tables.Contains("LOAD_ENTRIES");

                    return res;
                }
                catch (Exception exc)
                {
                    System.Windows.Forms.MessageBox.Show(exc.Message + "\n=========================\n" + exc.StackTrace, "ERROR!");
                    return false;
                }
                finally
                {
                    _connection.Close();
                }
            }
            else
            {
                throw new Exception("Not connected to any DB.");
            }
        }

        /// <summary>
        /// Проверяет, содержит ли БД такую запись.
        /// </summary>
        /// <param name="file">Название проверяемой записи</param>
        /// <returns>Булевый результат проверки.</returns>
        public bool ContainsEntry(string file)
        {
            List<string> FileEntrties = new List<string>(GetLoadFilesList());
            return FileEntrties.Contains(file);
        }

        #endregion

        #region DB: Getters

        /// <summary>
        /// Возвращает ID записи в указанной таблице по её значению.
        /// </summary>
        /// <param name="tableName">Имя таблицы, в которой производится поиск.</param>
        /// <param name="name">Строка по которой производится поиск.</param>
        /// <param name="argName">Название поля в БД.</param>
        /// <param name="close">Флаг, указывающий, закрывать ли за собой соединение (на случай вызова из функции с открытым соединением).</param>
        /// <returns>Искомый ID.</returns>
        private int GetIDByName(string tableName, string name, string argName = "name", bool close = false)
        {
            int resultId = -1;

            try
            {
                string sqlGetTeacherId = string.Format("SELECT id FROM {0} WHERE {1} = @data", tableName, argName);
                SQLiteCommand cmdGetTeacherId = new SQLiteCommand(sqlGetTeacherId, _connection);
                cmdGetTeacherId.Parameters.AddWithValue("@data", name);
                using (SQLiteDataReader dataReader = cmdGetTeacherId.ExecuteReader())
                { while (dataReader.Read()) { resultId = dataReader.GetInt32(0); } }
            }
            catch (Exception exc)
            {
                //Логирование
                System.Windows.Forms.MessageBox.Show(exc.Message + "\n=========================\n" + exc.StackTrace);
                return -1;
            }
            finally
            {
                if (close) { _connection.Close(); }
            }

            return resultId;
        }

        /// <summary>
        /// Возвращает наибольшее значение ID в указанной таблице.
        /// </summary>
        /// <param name="tableName">Имя таблицы, в которой производится поиск.</param>
        /// <param name="close">Флаг, указывающий, закрывать ли за собой соединение (на случай вызова из функции с открытым соединением).</param>
        /// <returns>Искомый ID.</returns>
        public int GetLastId(string tableName, bool close = false)
        {
            int Id = -1;

            string sqlGetLastID = string.Format("SELECT * FROM {0} ORDER BY id DESC LIMIT 1", tableName);

            try
            {
                if(_connection != null && _connection.State != System.Data.ConnectionState.Open) { _connection.Open(); }

                SQLiteCommand cmdGetLastId = new SQLiteCommand(sqlGetLastID, _connection);
                using (SQLiteDataReader dataReader = cmdGetLastId.ExecuteReader())
                {
                    while (dataReader.Read())
                    { Id = dataReader.GetInt32(0); }
                }
            }
            catch (Exception exc)
            {
                System.Windows.Forms.MessageBox.Show(exc.Message + "\n=========================\n" + exc.StackTrace);
            }
            finally
            {
                if(close) { _connection.Close(); }
            }

            return Id;
        }

        /// <summary>
        /// Получает список названий записей.
        /// </summary>
        /// <returns>Массив названий записей.</returns>
        public string[] GetLoadFilesList()
        {
            List<string> resultList = new List<string>();
            string sqlGetFilesData = "SELECT date, filename, version FROM FILES";

            try
            {
                _connection.Open();

                SQLiteCommand cmdGetFilesData = new SQLiteCommand(sqlGetFilesData, _connection);
                using (SQLiteDataReader dataReader = cmdGetFilesData.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        resultList.Add(
                            new FileTitle(
                                dataReader.GetString(0),
                                dataReader.GetString(1),
                                dataReader.GetInt32(2)
                                ).ToString()
                            );
                    }
                }
            }
            catch (Exception exc)
            {
                //Логирование
                System.Windows.Forms.MessageBox.Show(exc.Message + "\n=========================\n" + exc.StackTrace);
            }
            finally
            {
                _connection.Close();
            }

            return resultList.ToArray();
        }

        /// <summary>
        /// Получает запись по названию.
        /// </summary>
        /// <param name="fileTitle">Название записи</param>
        /// <param name="close">Флаг, указывающий, закрывать ли за собой соединение (на случай вызова из функции с открытым соединением).</param>
        /// <returns>Сформированная модель.</returns>
        public DataModel GetModelByTitle(FileTitle fileTitle, bool close = true)
        {
            DataModel resultModel = new DataModel(fileTitle);

            #region SQL
            string sqlGetFileId = "SELECT id FROM FILES WHERE filename = @filename AND date = @date AND version = @version";
            string sqlGetLoadEntriesIds = "SELECT FID_semester, FID_teacher, FID_subject, FID_student_flow, FID_study_type, hours FROM LOAD_ENTRIES WHERE FID_file_id = @FID_file_id";
            string sqlGetDataByFID = "SELECT name FROM {0} WHERE id = @id";
            #endregion

            #region Metadata
            List <LoadEntryIdItem> ModelComponentsIds = new List<LoadEntryIdItem>();
            #endregion

            #region Execution
            try
            {
                #region Открываем соединение
                if (
                    _connection != null &&
                    _connection.State != System.Data.ConnectionState.Open
                    )
                { _connection.Open(); }
                #endregion

                #region Получаем id файла.
                SQLiteCommand cmdGetFileId = new SQLiteCommand(sqlGetFileId, _connection);
                cmdGetFileId.Parameters.AddWithValue("@filename", fileTitle.FileName);
                cmdGetFileId.Parameters.AddWithValue("@date", fileTitle.Date);
                cmdGetFileId.Parameters.AddWithValue("@version", fileTitle.Version);
                using (SQLiteDataReader dataReader = cmdGetFileId.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        resultModel.Title.FileId = dataReader.GetInt32(0);
                    }
                }
                #endregion

                #region Получаем перечень id нагрузок из этого файла.
                SQLiteCommand cmdGetLoadEntriesIds = new SQLiteCommand(sqlGetLoadEntriesIds, _connection);
                cmdGetLoadEntriesIds.Parameters.AddWithValue("@FID_file_id", resultModel.Title.FileId);
                using (SQLiteDataReader dataReader = cmdGetLoadEntriesIds.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        ModelComponentsIds.Add(
                            new LoadEntryIdItem(
                                dataReader.GetInt32(0),
                                dataReader.GetInt32(2),
                                dataReader.GetInt32(1),
                                dataReader.GetInt32(4),
                                dataReader.GetInt32(3),
                                dataReader.GetInt32(5)
                                )
                            );
                    }
                }
                #endregion

                #region Получаем данные по нужным id.
                SQLiteCommand cmdGetSubjectData = new SQLiteCommand(string.Format(sqlGetDataByFID, "SUBJECTS"), _connection);
                SQLiteCommand cmdGetTeacherData = new SQLiteCommand(string.Format(sqlGetDataByFID, "TEACHERS"), _connection);
                SQLiteCommand cmdGetStudyTypeData = new SQLiteCommand(string.Format(sqlGetDataByFID, "STUDY_TYPES"), _connection);
                SQLiteCommand cmdGetStudentFlowData = new SQLiteCommand(string.Format(sqlGetDataByFID, "STUDENT_FLOWS"), _connection);

                int entryIndex = -1;
                foreach(var loadItem in ModelComponentsIds)
                {
                    cmdGetSubjectData.Parameters.AddWithValue("@id", loadItem.SubjectId);
                    cmdGetTeacherData.Parameters.AddWithValue("@id", loadItem.TeacherId);
                    cmdGetStudyTypeData.Parameters.AddWithValue("@id", loadItem.StudyTypeId);
                    cmdGetStudentFlowData.Parameters.AddWithValue("@id", loadItem.StudentFlowId);

                    resultModel.LoadEntries.Add(new LoadEntryItem());   entryIndex++;

                    resultModel.LoadEntries[entryIndex].Semester = loadItem.SemesterId;
                    resultModel.LoadEntries[entryIndex].Hours = loadItem.Hours;

                    using (var dataReader = cmdGetSubjectData.ExecuteReader())
                    { dataReader.Read(); resultModel.LoadEntries[entryIndex].Subject = dataReader.GetString(0); }
                    using (var dataReader = cmdGetTeacherData.ExecuteReader())
                    { dataReader.Read(); resultModel.LoadEntries[entryIndex].Teacher = dataReader.GetString(0); }
                    using (var dataReader = cmdGetStudyTypeData.ExecuteReader())
                    { dataReader.Read(); resultModel.LoadEntries[entryIndex].StudyType = dataReader.GetString(0); }
                    using (var dataReader = cmdGetStudentFlowData.ExecuteReader())
                    { dataReader.Read(); resultModel.LoadEntries[entryIndex].StudentFlow = dataReader.GetString(0); }
                }

                #endregion

                #region Заполняем служебные коллекции модели. 
                foreach (var loadEntry in resultModel.LoadEntries)
                {
                    //Предмет
                    if (!resultModel.Subjects.Contains(loadEntry.Subject)) { resultModel.Subjects.Add(loadEntry.Subject); }
                    //Преподаватель
                    if (!resultModel.Teachers.Contains(loadEntry.Teacher)) { resultModel.Teachers.Add(loadEntry.Teacher); }
                    //Тип работ
                    if (!resultModel.StudyTypes.Contains(loadEntry.StudyType)) { resultModel.StudyTypes.Add(loadEntry.StudyType); }
                    //Поток
                    if (!resultModel.StudentFlows.Contains(loadEntry.StudentFlow))
                    {
                        resultModel.StudentFlows.Add(loadEntry.StudentFlow);
                        
                        //Если нет потока, то нет и груп.
                        string[] groups = DataMapper.FlowToGroups(loadEntry.StudentFlow);
                        for (int i = 0; i < groups.Length; i++)
                        { if (!resultModel.Groups.Contains(groups[i])) { resultModel.Groups.Add(groups[i]); } }
                    }

                    //Часы
                    resultModel.Hours[loadEntry.StudyType] += loadEntry.Hours;
                }
                #endregion
            }
            catch (Exception exc)
            {
                System.Windows.Forms.MessageBox.Show(exc.Message + "\n=========================\n" + exc.StackTrace);
                return null;
            }
            finally
            {
                if (close) { _connection.Close(); }
            }
            #endregion

            return resultModel;
        }
        #endregion

        #region DB: Modifiers

        /// <summary>
        /// Обновляет даты в указанном файле на предоставленную.
        /// </summary>
        /// <param name="fileId">Идентификатор целевого файла в ДБ.</param>
        /// <param name="newDate">Новая дата</param>
        /// <param name="close">Флаг, указывающий, закрывать ли за собой соединение (на случай вызова из функции с открытым соединением).</param>
        /// <returns>Успешность операции</returns>
        public bool UpdateFileDate(int fileId, string newDate, bool close = false)
        {
            bool operationResult = false;

            #region SQL
            string sqlUpdateFileDate = "UPDATE FILES SET date = @newdate WHERE id = @id";
            #endregion

            #region Execution
            try
            {
                if (_connection != null && _connection.State != System.Data.ConnectionState.Open) { _connection.Open(); }

                SQLiteCommand cmdUpdateFileDate = new SQLiteCommand(sqlUpdateFileDate, _connection);

                cmdUpdateFileDate.Parameters.AddWithValue("@newdate", newDate);
                cmdUpdateFileDate.Parameters.AddWithValue("@id", fileId);

                operationResult = (cmdUpdateFileDate.ExecuteNonQuery() >= 1) ? true : false;
            }
            catch (Exception exc)
            {
                //Логирование
                System.Windows.Forms.MessageBox.Show(exc.Message + "\n=========================\n" + exc.StackTrace);
                return false;
            }
            finally
            {
                //Закрываем соединение
                if (close) { _connection.Close(); }
            }
            #endregion

            return operationResult;
        }

        /// <summary>
        /// Удаляет из ДБ запись с заданым названием
        /// </summary>
        /// <param name="title">Название записи</param>
        /// <returns></returns>
        public bool DeleteEntry(string fileTitle)
        {
            try
            {
                if (ContainsEntry(fileTitle))
                {
                    _connection.Open();
                    string sqlDeleteEntry = "DELETE FROM Reports WHERE title = @title";
                    SQLiteCommand cmdDeleteEntry = new SQLiteCommand(sqlDeleteEntry, _connection);
                    cmdDeleteEntry.Parameters.AddWithValue("@title", fileTitle);

                    cmdDeleteEntry.ExecuteNonQuery();
                }
                else
                {
                    return true;
                    //throw new Exception("An attempt to delete non-existing entry!");                    
                }
            }
            catch (Exception exc)
            {
                System.Windows.Forms.MessageBox.Show(exc.Message, "ERROR!");
                return true;
            }
            finally
            {
                _connection.Close();
            }

            return false;
        }

        #endregion

        #endregion
    }
}

﻿using System;
using System.Windows.Forms;

namespace UniversityBurden2dot0.Forms
{
    /// <summary>
    /// Форма-диалог, которая вызывается при отсутвии рабочей БД.
    /// </summary>
    public partial class DBDialogForm : Form
    {
        #region Fields

        /// <summary>
        /// Служебный флаг при закрытии формы без выбора варианта действий.
        /// </summary>
        private bool _canceling;

        /// <summary>
        /// Флаг сигнализирующий о выборе варианта действий.
        /// </summary>
        public bool CreateNew { get; private set; }

        /// <summary>
        /// Строка с полным путём к файлу БД.
        /// </summary>
        public string CustomDBPath { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Конструктор формы.
        /// </summary>
        public DBDialogForm()
        {
            //Системная инициализация
            InitializeComponent();

            //Инициализация всех флагов и полей.
            _canceling = true;
            CreateNew = false;
            CustomDBPath = "";
        }

        private void DBDialogForm_Load(object sender, EventArgs e)
        {
            //Установка визуала.
            this.Icon = System.Drawing.SystemIcons.Exclamation;
        }

        #endregion

        #region Event Handlers

        //Нажатие на кнопку создания новой БД.
        private void CreateDBButton_Click(object sender, EventArgs e)
        {
            CreateNew = true;

            _canceling = false;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        //Нажатие на кнопку выбора кастомного пути.
        private void ChooseDBButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog
            {
                Multiselect = false,
                Filter = "database files (*.db)|*.db|All files (*.*)|*.*",
                FilterIndex = 2,
                RestoreDirectory = true
            };

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                CustomDBPath = dlg.FileName;
                CreateNew = false;

                _canceling = false;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            { return; }
        }

        //Событие закрытия формы.
        private void DBDialogForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(this.DialogResult != DialogResult.Cancel && _canceling)
                this.DialogResult = DialogResult.Cancel;
        }

        #endregion
    }
}

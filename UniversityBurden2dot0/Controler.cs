﻿using System;
using System.Collections.Generic;
using UniversityBurden2dot0.Handlers;
using UniversityBurden2dot0.DataModels;

namespace UniversityBurden2dot0
{
    /// <summary>
    /// Контроллер приложения, осуществляющий комуникацию между программными слоями и сущностями.
    /// </summary>
    public class Controler
    {
        #region Fields

        #region Public Fields

        /// <summary>
        /// Содержимое выгруженного файла.
        /// </summary>
        /// <returns>Экзепляр DataSet, представляющий зеркало выгруженного файла.</returns>
        public System.Data.DataSet RawFileContent
        {
            get { return _xlsHandler.FileContent; }
        }

        /// <summary>
        /// Ссылка на модель данных.
        /// </summary>
        public SortedDictionary<string, DataModel> LoadedModels { get; private set; }

        #endregion

        #region Private Fields

        /// <summary>
        /// Ссылка на класс, работающий с таблицами MS Excel.
        /// </summary>
        private XLSHandler _xlsHandler;

        ///// <summary>
        ///// Ссылка на класс, работающий с документами MS Word.
        ///// </summary>
        private DOCHandler _docxHander;

        
        /// <summary>
        /// Ссылка на класс, работающий с базой данных SQLite.
        /// </summary>
        private SQLiteHandler _sqliteHandler;


        /// <summary>
        /// Ссылка на класс, обрабатывающий данные и приводящий их к необходимому виду.
        /// </summary>
        private DataMapper _mapper;

        /// <summary>
        /// Хранит список последних загруженных файлов.
        /// </summary>
        private string[] _lastImportedPaths;

        #endregion

        #endregion

        #region Events and Delegates

        /// <summary>
        /// Делегат для смены настроек
        /// </summary>
        public delegate void SettingsChangedHandler(object sender, Utils.SettingsEventArgs eventArgs);

        /// <summary>
        /// Событие при изменении настроек (не текущем, а сохранённом).
        /// </summary>
        public event SettingsChangedHandler OnSettingsChanged;


        #endregion

        #region Event Handlers

        #endregion

        #region Constructor and Init Check

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        public Controler()
        {
            //Инициализация контейнера данных и маппера
            LoadedModels = new SortedDictionary<string, DataModel>();
            _mapper = new DataMapper();

            //Инициализация хендлеров данных
            _xlsHandler = new XLSHandler();
            _docxHander = new DOCHandler();
            _sqliteHandler = new SQLiteHandler();

            //Подписка на события

        }

        /// <summary>
        /// Первичная проверка БД.
        /// </summary>
        /// <returns>Результат проверки.</returns>
        public bool InitCheck()
        {
            try
            {
                return _sqliteHandler.InitCheck();
            }
            catch (Exception exc)
            {
                //заменить на логирование
                System.Windows.Forms.MessageBox.Show(exc.Message + "\n=========================\n" + exc.StackTrace);
                /////////
                return false;
            }
        }

        #endregion

        #region General

        public void ChangeSettings()
        {
            Forms.SettingsForm settingsForm = new Forms.SettingsForm();
            if(settingsForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //локальная обработка изменений

                //Сообщение всем зависящим от настроек компонентам.
                OnSettingsChanged?.Invoke(this, new Utils.SettingsEventArgs(settingsForm.DBPathChanged));
            }
        }

        #endregion

        #region Xls-related

        /// <summary>
        /// Производит принудительное закрытие Excell и освобождение всех связаных ресурсов.
        /// </summary>
        public void ForceCloseExcelApp()
        {
            _xlsHandler.ForceCloseApp();
        }

        #region File-related

        /// <summary>
        /// Обрабатывает xls-документ и импортирует данные в ДБ.
        /// </summary>
        /// <returns>Успешность операции.</returns>
        public bool ImportXLSLoad()
        {
            bool result = false;
            int pathIndex = -1;

            //Этап выбора файлов
            BrowseXLSFileToOpen();

            if(_lastImportedPaths != null)
            {
                foreach (string path in _lastImportedPaths)
                {
                    pathIndex++;

                    //Импорт из Экселя
                    if (LoadFileContent(path))
                    {
                        //Этап обработки сырых данных и последующего определения версии
                        DataModel modelToAdd = _mapper.ProccessRawDataSet(_xlsHandler.FileContent);

                        //Флаг об отсутсвии версий файла
                        bool noSuchFile = true;
                        //Флаг об обновдении даты
                        bool updateDate = false;
                        //Файл в котором обновить дату
                        FileTitle modelTitleToUpdate = new FileTitle();

                        //Ищем и сравниваем
                        if (LoadedModels.Count == 0)
                        {
                            noSuchFile = true;
                            modelToAdd.Title.Version = 0;
                        }
                        else
                        {
                            foreach (var model in LoadedModels.Values)
                            {
                                //Сравниваем только версии того же файла
                                if (model.Title.FileName != modelToAdd.Title.FileName)
                                {
                                    continue;
                                }
                                else
                                {
                                    //Нашли хотя бы один такой файл
                                    if (!noSuchFile) noSuchFile = false;

                                    //Сравниваем содержимое
                                    if (model.CompareContent(modelToAdd))
                                    {
                                        //Если совпадает -- запонимаем его название и выставляем флаг на обновление даты
                                        updateDate = true;
                                        noSuchFile = false;
                                        modelTitleToUpdate = model.Title;
                                        break;
                                    }
                                    else
                                    {
                                        //Если нет -- увеличиваем версию и продолжаем шерстить.
                                        modelToAdd.Title.Version = model.Title.Version + 1;
                                    }
                                }
                            }
                        }

                        //Определяем закрывать ли за собой соединение
                        //(Если файл последний в очереди
                        bool close = (pathIndex == _lastImportedPaths.GetUpperBound(0)) ? true : false;

                        // Этап импотрирования обработанных данных в общую модель и БД

                        //Такого файла вообще нет или он есть но отличается версией
                        if (noSuchFile || !updateDate)
                        {
                            //Добавили в ДБ и изнали ID файла
                            result = _sqliteHandler.InsertDataModel(modelToAdd, out int id, close);
                            //Присвоили ID
                            modelToAdd.Title.FileId = id;
                            //Добавили в модель
                            LoadedModels.Add(modelToAdd.Title.ToString(), modelToAdd);
                        }
                        //Файл есть и ему надо обновить дату
                        else
                        {
                            //Обновляем дату и передобавляем в модель
                            var modelToUpdate = LoadedModels[modelTitleToUpdate.ToString()];
                            LoadedModels.Remove(modelToUpdate.Title.ToString());
                            modelToUpdate.Title.Date = modelToAdd.Title.Date;
                            LoadedModels.Add(modelToUpdate.Title.ToString(), modelToUpdate);

                            //Обновляем дату в БД
                            result = _sqliteHandler.UpdateFileDate(modelToUpdate.Title.FileId, modelToAdd.Title.Date, close);
                        }
                    }
                    else
                    { return false; }
                }
            }
            else { result = true; }

            return result;
        }

        /// <summary>
        /// Открывает диалог выбора файла.
        /// </summary>
        /// <returns>Строка, содержащая полный путь к файлу. возвращает пустую строку в случае отмены или ощибки.</returns>
        public void BrowseXLSFileToOpen()
        {          
            System.Windows.Forms.OpenFileDialog OpenDlg = new System.Windows.Forms.OpenFileDialog()
            {
                Multiselect = true,
                Filter = "xls files (*.xls)|*.xls|xlsx files (*.xlsx)|*.xlsx|All files (*.*)|*.*",
                FilterIndex = 3,
                RestoreDirectory = true
            };

            if (OpenDlg.ShowDialog() != System.Windows.Forms.DialogResult.OK)
            { _lastImportedPaths = null; }
            else
            { _lastImportedPaths = OpenDlg.FileNames; }
        }

        /// <summary>
        /// Выгружает содержимое открытого файла в эквивалентную программную сущность.
        /// </summary>
        /// <returns>Успешность операции.</returns>
        public bool LoadFileContent(string filePath)
        {
            return _xlsHandler.LoadFileContent(filePath);
        }

        #endregion

        #endregion

        #region Doc-related

        public bool ExportToDoc()
        {
            return false;
        }

        #endregion

        #region SQlite-related

        /// <summary>
        /// Возвращает список загруженых файлов нагрузки
        /// </summary>
        /// <returns></returns>
        public string[] UpdateLoadFilesList()
        {
            return _sqliteHandler.GetLoadFilesList();
        }

        /// <summary>
        /// Возвращает модель по названию или загружает соотвю модель из БД.
        /// </summary>
        /// <param name="name">Название искомой модели.</param>
        /// <returns>Искомая модель.</returns>
        public DataModel LoadToModel(string name)
        {
            if (!LoadedModels.ContainsKey(name))
            { LoadedModels.Add(name, _sqliteHandler.GetModelByTitle(new FileTitle(name))); }

            return LoadedModels[name];
        }

        #endregion
    }
}

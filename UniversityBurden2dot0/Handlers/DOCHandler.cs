﻿using System;
using System.Collections.Generic;

using Word = Microsoft.Office.Interop.Word;
using Marshal = System.Runtime.InteropServices.Marshal;

namespace UniversityBurden2dot0.Handlers
{
    /// <summary>
    /// Класс, осуществляющий взаимодействие с DOC-документами через скрытый экземпляр Microsoft Word.
    /// </summary>
    public  class DOCHandler
    {
        #region Fields

        private Word.Application _wordApp = null;

        private Word.Document _doc = null;

        private object _savePath = null;

        #endregion

        #region Constructor

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        /// <param name="savePath"></param>
        public DOCHandler(string savePath = null)
        {
            _wordApp = new Word.Application
            { Visible = false };

            if (savePath != null) _savePath = savePath;
        }

        #endregion

        #region Export

        /// <summary>
        /// Экспортирует переданные данные в документ Microsoft Word.
        /// </summary>
        /// <param name="data">Данные для экспорта.</param>
        /// <returns>Успешнсть операции.</returns>
        public bool ExportToDoc(object data)
        {
            return false;
        }

        #endregion

        #region Static Export

        /// <summary>
        /// Статический метод экспорта данных в документ Microsoft Word.
        /// </summary>
        /// <param name="pathToSaveInto">Путь сохранения документа.</param>
        /// <param name="dataToExport">Данные на экспорт.</param>
        /// <returns>Успешность операции.</returns>
        public static bool StaticExportToDoc(string pathToSaveInto, List<Tuple<string, int, string>> dataToExport )
        {
            #region Meta-Data
            //Список категорий
            Dictionary<string, string> workTypesCategories = new Dictionary<string, string>
            {
                { "Розробка робочих програм з дисципліни", "Навчально-методична робота" },
                { "Переробка робочих навчальних програм", "Навчально-методична робота" },
                { "Переробка програм на виробничу практику", "Навчально-методична робота" },
                { "Розробка та підготовка до видання (друкованих) електроних підручників", "Навчально-методична робота" },
                { "Підготовка навчально-методичних матеріалів для видання", "Навчально-методична робота" },
                { "Підготовка до проведення лекцій для мультимедійної аудиторії", "Навчально-методична робота" },
                { "Підготовка до проведення лабораторних робіт", "Навчально-методична робота" },
                { "Розробка та впровадження нових лабораторних робіт з використанням існуючого ПЗ", "Навчально-методична робота" },
                { "Розробка та впровадження нових лабораторних робіт з використанням нового ПЗ", "Навчально-методична робота" },
                { "Складання екзаменаційних білетів", "Навчально-методична робота" },
                { "Складання комплекту типових контрольних завданнь з навчальної дисципліни", "Навчально-методична робота" },

                { "Використання інформаційних технологій при керуванні дипломним проектуванням", "Навчально-методична робота" },
                { "Освоєння нових програмних оболонок", "Навчально-методична робота"},

                { "Розробка компютерних тестів з навчальної дисципліни", "Навчально-методична робота" },
                { "Доопрацьовування тестів (0,5 від розробки)", "Навчально-методична робота" },


                { "Розробка завдань на дипломний проект", "Розробка завдань" },
                { "Розробка завдань на бакалаврську роботу", "Розробка завдань" },
                { "Розробка завдань на курсовий проект", "Розробка завдань" },
                { "Розробка завдань на домашню роботу", "Розробка завдань" },


                { "Написання статей", "Наукова робота" },
                { "Підготовка тез доповідей", "Наукова робота" },
                { "Керівництво науковою роботою студентів з підготовкою доповіді на конференцію", "Наукова робота" },
                { "Пошук електроних джерел", "Наукова робота" },


                { "Керівництво студентським гуртком", "Виховна робота" },
                { "Виконання обов’язків куратора", "Виховна робота" }
            };

            //Состояние категорий (содержит-несодержит)
            Dictionary<string, bool> CategoryState = new Dictionary<string, bool>
            {
                {"Навчально-методична робота", false },
                {"Тестування", false },
                {"Розробка завдань", false },
                {"Наукова робота", false },
                {"Виховна робота", false }
            };

            //Служебные объекты
            object Missing = System.Reflection.Missing.Value;
            object saveLocation = pathToSaveInto;

            //Объекты приложения и документа
            Word.Application wordApp = null;
            Word.Document doc = null;
            #endregion

            try
            {
                //Открываем приложение и добавляем новый документ
                wordApp = new Word.Application
                { Visible = false };
                doc = wordApp.Documents.Add(Missing, Missing, Missing, Missing);

                doc.Content.SetRange(0, 0);
                object tightStyle = "Без интервала";
                

                //Заголовок
                Word.Paragraph headerPara = doc.Content.Paragraphs.Add(ref Missing);
                headerPara.Range.Text = "Нотатки до індивідуального плану";

                headerPara.Range.set_Style(ref tightStyle);

                headerPara.Range.Font.ColorIndex = Word.WdColorIndex.wdBlack;
                headerPara.Range.Font.Bold = 1;
                headerPara.Range.Font.Size = 14;
                headerPara.Range.Font.Name = "Times New Roman";

                headerPara.Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
                headerPara.Range.InsertParagraphAfter();


                //Содержание
                foreach (var item in dataToExport)
                {
                    if(!CategoryState[workTypesCategories[item.Item1]])
                    {
                        CategoryState[workTypesCategories[item.Item1]] = true;

                        //Разделитель
                        Word.Paragraph sepPara = doc.Paragraphs.Add(ref Missing);
                        sepPara.Range.Text = "==============================================";

                        sepPara.Range.Font.Bold = 0;
                        sepPara.Range.Font.Size = 14;
                        sepPara.Range.Font.Name = "Times New Roman";
                        sepPara.Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;

                        sepPara.Range.InsertParagraphAfter();
                        sepPara.Range.Font.Underline = Word.WdUnderline.wdUnderlineSingle;


                        //Категория
                        Word.Paragraph categoryPara = doc.Paragraphs.Add(ref Missing);
                        categoryPara.Range.Text = workTypesCategories[item.Item1] + Environment.NewLine;

                        categoryPara.Range.InsertParagraphAfter();
                        categoryPara.Range.Font.Underline = Word.WdUnderline.wdUnderlineNone;
                    }

                    string paraText =
                        item.Item1 + ": " + item.Item2 + Environment.NewLine;
                    if(item.Item3 != " ")
                    {
                        paraText +=
                        "Коментар: " + item.Item3 + Environment.NewLine;
                    }
                    paraText += Environment.NewLine;

                    Word.Paragraph contentPara = doc.Paragraphs.Add(ref Missing);
                    contentPara.Range.Text = paraText;

                    contentPara.Range.Font.Underline = Word.WdUnderline.wdUnderlineNone;
                    contentPara.Range.Font.Underline = Word.WdUnderline.wdUnderlineNone;
                    contentPara.Range.Font.Size = 14;
                    contentPara.Range.Font.Name = "Times New Roman";

                    contentPara.Range.InsertParagraphAfter();
                }

                //Нижний колонтитул
                Word.Paragraph sepFooterPara = doc.Paragraphs.Add(ref Missing);
                sepFooterPara.Range.Text = "==============================================";

                sepFooterPara.Range.InsertParagraphAfter();

                Word.Paragraph footerPara = doc.Content.Paragraphs.Add(ref Missing);
                footerPara.Range.Text = "ХНУРЕ " + DateTime.Now.Year.ToString();

                footerPara.Range.Font.Size = 10;
                footerPara.Range.Font.Name = "Times New Roman";
                footerPara.Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;

                footerPara.Range.InsertParagraphAfter();


                //Сохраняем сформированный документ
                doc.SaveAs2(ref saveLocation);
                return true;
            }
            catch (Exception exc)
            {
                System.Windows.Forms.MessageBox.Show(
                    exc.Message +
                    "\n=================================\n" +
                    exc.StackTrace);
                return false;
            }
            finally
            {
                //Заквываем и подчищаем за собой
                doc.Close(Missing, Missing, Missing);
                Marshal.ReleaseComObject(doc);
                doc = null;
                wordApp.Quit(Missing, Missing, Missing);
                Marshal.ReleaseComObject(wordApp);
                wordApp = null;
            }
        }

        #endregion
    }
}

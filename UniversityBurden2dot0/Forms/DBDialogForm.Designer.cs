﻿namespace UniversityBurden2dot0.Forms
{
    partial class DBDialogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DBDialogForm));
            this.CreateDBButton = new System.Windows.Forms.Button();
            this.ChooseDBButton = new System.Windows.Forms.Button();
            this.InfoLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // CreateDBButton
            // 
            this.CreateDBButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CreateDBButton.Location = new System.Drawing.Point(66, 122);
            this.CreateDBButton.Name = "CreateDBButton";
            this.CreateDBButton.Size = new System.Drawing.Size(295, 39);
            this.CreateDBButton.TabIndex = 0;
            this.CreateDBButton.Text = "Створити нову Базу Даних";
            this.CreateDBButton.UseVisualStyleBackColor = true;
            this.CreateDBButton.Click += new System.EventHandler(this.CreateDBButton_Click);
            // 
            // ChooseDBButton
            // 
            this.ChooseDBButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ChooseDBButton.Location = new System.Drawing.Point(66, 167);
            this.ChooseDBButton.Name = "ChooseDBButton";
            this.ChooseDBButton.Size = new System.Drawing.Size(295, 39);
            this.ChooseDBButton.TabIndex = 1;
            this.ChooseDBButton.Text = "Вказати шлях до існуючої";
            this.ChooseDBButton.UseVisualStyleBackColor = true;
            this.ChooseDBButton.Click += new System.EventHandler(this.ChooseDBButton_Click);
            // 
            // InfoLabel
            // 
            this.InfoLabel.AutoSize = true;
            this.InfoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.InfoLabel.Location = new System.Drawing.Point(12, 9);
            this.InfoLabel.Name = "InfoLabel";
            this.InfoLabel.Size = new System.Drawing.Size(366, 108);
            this.InfoLabel.TabIndex = 0;
            this.InfoLabel.Text = resources.GetString("InfoLabel.Text");
            this.InfoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DBDialogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(389, 214);
            this.Controls.Add(this.InfoLabel);
            this.Controls.Add(this.ChooseDBButton);
            this.Controls.Add(this.CreateDBButton);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DBDialogForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Увага!";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DBDialogForm_FormClosing);
            this.Load += new System.EventHandler(this.DBDialogForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CreateDBButton;
        private System.Windows.Forms.Button ChooseDBButton;
        private System.Windows.Forms.Label InfoLabel;
    }
}
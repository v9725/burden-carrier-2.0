﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;

namespace UniversityBurden2dot0
{
    /// <summary>
    /// Главное окно пользовательского интерфейса.
    /// </summary>
    public partial class MainForm : Form
    {
        #region Fields

        /// <summary>
        /// Контролер формы для связи с другими слоями приложения.
        /// </summary>
        private readonly Controler _controler;

        /// <summary>
        /// ViewModel.
        /// </summary>
        private DataModels.DataModel _viewModel;

        #endregion

        #region Constructor and Closing Event Handler

        /// <summary>
        /// Конструктор формы.
        /// </summary>
        public MainForm(Controler controler)
        {
            //Инициализация компонентов
            InitializeComponent();

            //Внедрение контролера
            _controler = controler;
            _viewModel = new DataModels.DataModel("");

            //Подписка на события
            _controler.OnSettingsChanged += OnSettingsChanged_Handler;
            
            #region Visuals

            //Настраиваем тултипы
            ToolTip newfileTT = new ToolTip();
            newfileTT.SetToolTip(AddLoadButton, "Додати новий файл навантаження");

            ToolTip refreshTT = new ToolTip();
            refreshTT.SetToolTip(UpdateFilesButton, "Оновити список файлів");

            ToolTip clearTT = new ToolTip();
            clearTT.SetToolTip(ClearContentButton, "Закрити активне навантаження");

            ToolTip secondhalfTT = new ToolTip();
            secondhalfTT.SetToolTip(SecondHalfButton, "Сформувати другу половину дня");

            ToolTip settingTT = new ToolTip();
            settingTT.SetToolTip(SettingsButton, "Налаштування програми");

            ToolTip aboutTT = new ToolTip();
            aboutTT.SetToolTip(AboutButton, "Про програму");

            //Настраиваем иконки
            this.Icon = Properties.Resources.lossfull_app_icon;
            AddLoadButton.BackgroundImage = Properties.Resources.plus_icon.ToBitmap();
            UpdateFilesButton.BackgroundImage = Properties.Resources.refresh_icon.ToBitmap();
            ClearContentButton.BackgroundImage = Properties.Resources.clear_icon.ToBitmap();
            SecondHalfButton.BackgroundImage = Properties.Resources.edit_icon.ToBitmap();
            SettingsButton.BackgroundImage = Properties.Resources.settings_icon.ToBitmap();

            #endregion
        }

        #endregion

        #region Event Handlers

        #region EH: Loading

        /// <summary>
        /// При визуальной загрузке Формы
        /// </summary>
        private void MainForm_Load(object sender, EventArgs e)
        {
            //Отключаем контролы на время загрузки
            DBEntriesListBox.Enabled = false;
            AddLoadButton.Enabled = false;
            UpdateFilesButton.Enabled = false;

            //Подтягиваем список файлов
            UpdateFilesList();

            //Включаем контроллы обратно
            AddLoadButton.Enabled = true;
            UpdateFilesButton.Enabled = true;


            //Сброс состояний контролов
            ClearContentButton.Enabled = false;
            SecondHalfButton.Enabled = false;
        }

        #endregion

        #region EH: Buttons

        /// <summary>
        /// Загружает содержимое файла, выводит первую таблицу, настраивает кнопки переключения.
        /// </summary>
        private void AddLoadButton_Click(object sender, EventArgs e)
        {
            //Отключаем контролы на время загрузки
            DBEntriesListBox.Enabled = false;
            AddLoadButton.Enabled = false;
            UpdateFilesButton.Enabled = false;
            SecondHalfButton.Enabled = false;

            //Если выгрузка прошла успешно
            if (_controler.ImportXLSLoad())
            {
                UpdateFilesList();

                //Включаем контроллы обратно
                AddLoadButton.Enabled = true;
                UpdateFilesButton.Enabled = true;
            }
            else
            {
                //Заменить на логирование
                MessageBox.Show("Something went wrong during file loading!", "Error!");
            }
        }

        /// <summary>
        /// Обратбатывает нажатие кнопки закрытия отображаемой талицу.
        /// </summary>
        private void ClearContentButton_Click(object sender, EventArgs e)
        {
            ClearActiveLoadEntry();
        }

        /// <summary>
        /// Обрабатывает нажатие кнопки второй половины дня.
        /// </summary>
        private void SecondHalfButton_Click(object sender, EventArgs e)
        {
            DataModels.DataModel modelToFormFrom = _controler.LoadedModels[DBEntriesListBox.SelectedItem.ToString()];
            Forms.SecondHalfMenuForm secondHalfForm = new Forms.SecondHalfMenuForm(modelToFormFrom);
            secondHalfForm.ShowDialog();
        }


        /// <summary>
        /// Открывает окно настроек.
        /// </summary>
        private void SettingsButton_Click(object sender, EventArgs e)
        {
            _controler.ChangeSettings();
        }

        /// <summary>
        /// Открывает окно "О Программе".
        /// </summary>
        private void AboutButton_Click(object sender, EventArgs e)
        {
            Forms.AboutForm form = new Forms.AboutForm();
            form.ShowDialog();
        }

        #endregion

        #region EH: Files List

        /// <summary>
        /// Обработчик изменения выбраднного индекса
        /// </summary>
        private void DBEntriesListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(DBEntriesListBox.SelectedIndex != -1)
            {
                LoadEntry(DBEntriesListBox.SelectedItem.ToString());

                ClearContentButton.Enabled = true;
                SecondHalfButton.Enabled = true;
                HideEmptyLoadEntriesCheckBox.Enabled = true;
            }
        }

        /// <summary>
        /// Обработчик повторного запроса списка файлов нагрузки.
        /// </summary>
        private void UpdateFilesButton_Click(object sender, EventArgs e)
        {
            UpdateFilesList();
        }

        #endregion

        #region EH: Controler

        private void OnSettingsChanged_Handler(object sender, Utils.SettingsEventArgs eventArgs)
        {
            if (eventArgs.DBPathChanged)
            {
                MessageBox.Show("Для приминения настроек требуеться перезапустить приложение!", "Внимание!", MessageBoxButtons.OK);
                Application.Restart();
            }
        }

        #endregion

        /// <summary>
        /// Обрабатывает смену параметра "Спрятать пустые записи".
        /// </summary>
        private void HideEmptyLoadEntriesCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (DBEntriesListBox.SelectedIndex != -1)
            {
                LoadEntry(DBEntriesListBox.SelectedItem.ToString());
            }
        }

        /// <summary>
        /// Обрабатывает смену параметра "Показать номера стоблцов"
        /// </summary>
        private void ShowRowsNumberCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (DBEntriesListBox.SelectedIndex != -1)
            {
                LoadEntry(DBEntriesListBox.SelectedItem.ToString());
            }
        }

        #endregion

        #region Logic

        /// <summary>
        /// Повторно запрашивает список файлов нагрузки, сохраняя выделение (если файл не удалён).
        /// </summary>
        private void UpdateFilesList()
        {
            string selectedItem = "";
            string[] files;

            //Если выделен файл
            if (DBEntriesListBox.SelectedIndex != -1)
            {
                //Сохраняем выделеный файл и сбрасываем выдиление
                selectedItem = DBEntriesListBox.SelectedItem.ToString();
                DBEntriesListBox.ClearSelected();
            }

            //Чистим список файлов
            DBEntriesListBox.Items.Clear();

            //Загружаем новый
            files = _controler.UpdateLoadFilesList();

            //Если БД не пуста
            if (files.Length > 0)
            {
                //Сгружаем список файлов
                DBEntriesListBox.Items.AddRange(files);

                //Загружаем все файлы из БД в программу.
                foreach (string file in DBEntriesListBox.Items)
                { _controler.LoadToModel(file); }
                DBEntriesListBox.Enabled = true;
            }
            //В противном случае
            else
            {
                //Пишем что ничего нет
                DBEntriesListBox.Items.Add("No entries yet...");
                DBEntriesListBox.Enabled = false;
            }

            //Если что-то было выбрано
            if (selectedItem != "")
            {
                //Определяем индекс выбранного в новом списке
                int index = DBEntriesListBox.Items.IndexOf(selectedItem);
                //Если такой файл ещё есть
                if (index != -1)
                {
                    //выбираем
                    DBEntriesListBox.SetSelected(index, true);
                }
            }

            //Обновляем вид
            DBEntriesListBox.Refresh();
        }

        /// <summary>
        /// Загружает Модель(и) во ViewModel и привязывает его к GridView.
        /// </summary>
        private void LoadEntry(string title)
        {
            //Адрессуем и подваязываем нужную модель из перечня.
            //(технически загрузка из БД рудиментна, но пусть пока будет)
            //(Дописать вливание)
            if (_controler.LoadedModels.ContainsKey(title))
            {
                _viewModel = _controler.LoadedModels[title];
            }
            else
            {
                _viewModel = _controler.LoadToModel(title);
            }
            
            //Применяем алиасы
            foreach (var loadEntry in _viewModel.LoadEntries)
            { loadEntry.Subject = Utils.AliasDictionary.Alias(loadEntry.Subject); }

            //Подвязываем основную таблицу
            WorkSheetDataGrid.AutoGenerateColumns = true;
            WorkSheetDataGrid.DataSource = _viewModel.LoadEntries;

            //Настраиваем стиль колонок основной таблицы
            foreach (DataGridViewColumn column in WorkSheetDataGrid.Columns)
            {
                switch (column.Name)
                {
                    case "Semester":
                        column.HeaderText = "Семестр";
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
                        break;
                    case "Hours":
                        column.HeaderText = "Часы";
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
                        break;

                    case "StudyType":
                        column.HeaderText = "Вид Работы";
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                        break;
                    case "Teacher":
                        column.HeaderText = "Преподаватель";
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                        break;

                    //case "Subject":
                    //    column.HeaderText = "Дициплина";
                    //    column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    //    break;
                    //case "StudentFlow":
                    //    column.HeaderText = "Учебный поток";
                    //    column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    //    break;
                    default:
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        break;
                }
                column.Resizable = DataGridViewTriState.True;
            }

            //Сбрассываем любое выделение
            WorkSheetDataGrid.CurrentCell = null;
            StatsDataGridView.CurrentCell = null;

            //Подготавливаем контейнер для статистики
            int amountStudyType = 12;
            List<int> first_semester = new List<int>(amountStudyType);
            List<int> second_semester = new List<int>(amountStudyType);
            for (int i = 0; i < amountStudyType; i++)
            {
                first_semester.Add(0);
                second_semester.Add(0);
            }

            //Настраиваем стиль строк основной таблицы и не только
            int rowindex = 1; //Переменная для номера строки
            foreach (DataGridViewRow row in WorkSheetDataGrid.Rows)
            {
                row.Resizable = DataGridViewTriState.False;

                //Скрываем ненужные строки при условии
                if (HideEmptyLoadEntriesCheckBox.Checked && 
                    (int)row.Cells["Hours"].Value == 0)
                {
                    row.Visible = false;
                }
                else
                {
                    row.Visible = true;

                    //Показываем номер видимых строк при условии
                    if (ShowRowsNumberCheckBox.Checked)
                    {
                        row.HeaderCell.Value = string.Format("{0}", rowindex++);
                    }
                    else
                    {
                        row.HeaderCell.Value = "";
                    }

                    //Собираем статистику для первого семестра таблицы статистики
                    if ((int)row.Cells["Semester"].Value == 1)
                    {
                        switch (row.Cells["StudyType"].Value.ToString())
                        {
                            #region ЛК
                            case "Лекції":
                                first_semester[0] += (int)row.Cells["Hours"].Value;
                                break;
                            #endregion

                            #region ПР
                            case "Практичні Роботи":
                                first_semester[1] += (int)row.Cells["Hours"].Value;
                                break;
                            #endregion

                            #region ЛБ
                            case "Лабораторні Роботи":
                                first_semester[2] += (int)row.Cells["Hours"].Value;
                                break;
                            #endregion

                            #region Конс
                            case "Консультації":
                                first_semester[3] += (int)row.Cells["Hours"].Value;
                                break;
                            #endregion

                            #region КП/КР
                            case "КП/КР":
                                first_semester[4] += (int)row.Cells["Hours"].Value;
                                break;
                            #endregion

                            #region КР/РГЗ
                            case "Контр. робіт і РГЗ":
                                first_semester[5] += (int)row.Cells["Hours"].Value;
                                break;
                            #endregion

                            #region Семестр. кр.
                            case "Семестровий контроль":
                                first_semester[6] += (int)row.Cells["Hours"].Value;
                                break;
                            #endregion

                            #region Дипл.
                            case "Дипломування":
                                first_semester[7] += (int)row.Cells["Hours"].Value;
                                break;
                            #endregion

                            #region ЕКЗ
                            case "ЕК":
                                first_semester[8] += (int)row.Cells["Hours"].Value;
                                break;
                            #endregion

                            #region Асп.
                            case "Аспірантура":
                                first_semester[9] += (int)row.Cells["Hours"].Value;
                                break;
                            #endregion

                            #region Маг.
                            case "Магістратура":
                                first_semester[10] += (int)row.Cells["Hours"].Value;
                                break;
                            #endregion

                            #region Практ.
                            case "Практика":
                                first_semester[11] += (int)row.Cells["Hours"].Value;
                                break;
                                #endregion
                        }
                    }
                    //Собираем статистику для второго семестра таблицы статистики
                    else
                    {
                        switch (row.Cells["StudyType"].ToString())
                        {
                            #region ЛК
                            case "Лекції":
                                second_semester[0] += (int)row.Cells["Hours"].Value;
                                break;
                            #endregion

                            #region ПР
                            case "Практичні Роботи":
                                second_semester[1] += (int)row.Cells["Hours"].Value;
                                break;
                            #endregion

                            #region ЛБ
                            case "Лабораторні Роботи":
                                second_semester[2] += (int)row.Cells["Hours"].Value;
                                break;
                            #endregion

                            #region Конс
                            case "Консультації":
                                second_semester[3] += (int)row.Cells["Hours"].Value;
                                break;
                            #endregion

                            #region КП/КР
                            case "КП/КР":
                                second_semester[4] += (int)row.Cells["Hours"].Value;
                                break;
                            #endregion

                            #region КР/РГЗ
                            case "Контр. робіт і РГЗ":
                                second_semester[5] += (int)row.Cells["Hours"].Value;
                                break;
                            #endregion

                            #region Семестр. кр.
                            case "Семестровий контроль":
                                second_semester[6] += (int)row.Cells["Hours"].Value;
                                break;
                            #endregion

                            #region Дипл.
                            case "Дипломування":
                                second_semester[7] += (int)row.Cells["Hours"].Value;
                                break;
                            #endregion

                            #region ЕКЗ
                            case "ЕК":
                                second_semester[8] += (int)row.Cells["Hours"].Value;
                                break;
                            #endregion

                            #region Асп.
                            case "Аспірантура":
                                second_semester[9] += (int)row.Cells["Hours"].Value;
                                break;
                            #endregion

                            #region Маг.
                            case "Магістратура":
                                second_semester[10] += (int)row.Cells["Hours"].Value;
                                break;
                            #endregion

                            #region Практ.
                            case "Практика":
                                second_semester[11] += (int)row.Cells["Hours"].Value;
                                break;
                                #endregion
                        }
                    }
                }
            }

            //Предварительно чистим талицу
            StatsDataGridView.Rows.Clear();
            StatsDataGridView.Columns.Clear();

            //Добавляем столбцы в таблицы статистики
            StatsDataGridView.Columns.Add("StatsColumn_SemesterSpace",  " ");
            StatsDataGridView.Columns.Add("StatsColumn_Lectures",       "Лекції");
            StatsDataGridView.Columns.Add("StatsColumn_Assignments", "Практичні");
            StatsDataGridView.Columns.Add("StatsColumn_LabAssignments", "Лабораторні");
            StatsDataGridView.Columns.Add("StatsColumn_Consulting", "Консультації");
            StatsDataGridView.Columns.Add("StatsColumn_KP_KR", "КП/КР");
            StatsDataGridView.Columns.Add("StatsColumn_KR_RGZ", "КР і РГЗ");
            StatsDataGridView.Columns.Add("StatsColumn_SemesterCheck", "Семестровий контроль");
            StatsDataGridView.Columns.Add("StatsColumn_Diplomas", "Дипломування");
            StatsDataGridView.Columns.Add("StatsColumn_Exams", "ЕК");
            StatsDataGridView.Columns.Add("StatsColumn_Aspirants", "Аспірантура");
            StatsDataGridView.Columns.Add("StatsColumn_Masters", "Магістратура");
            StatsDataGridView.Columns.Add("StatsColumn_SummerPractise", "Практика");


            //Добавляем строки в таблицы статистики и заполняем их
            DataGridViewRow firstRow = new DataGridViewRow();
            firstRow.Cells.Add(new DataGridViewTextBoxCell { Value = "I Семестр" });
            foreach(int item in first_semester)
            {
                DataGridViewCell cell = new DataGridViewTextBoxCell
                { Value = item };
                firstRow.Cells.Add(cell);
            }
            StatsDataGridView.Rows.Add(firstRow);

            DataGridViewRow secondRow = new DataGridViewRow();
            secondRow.Cells.Add(new DataGridViewTextBoxCell { Value = "II Семестр" });
            foreach (int item in second_semester)
            {
                DataGridViewCell cell = new DataGridViewTextBoxCell
                { Value = item };
                secondRow.Cells.Add(cell);
            }
            StatsDataGridView.Rows.Add(secondRow);

            //Настраиваем стиль колонок таблицы статистики
            foreach (DataGridViewColumn column in StatsDataGridView.Columns)
            { column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill; }

            //Перерисовываем
            WorkSheetDataGrid.Refresh();
            StatsDataGridView.Refresh();
        }

        /// <summary>
        /// Закрывает отображаемую талицу и сбрассывет выделение файла.
        /// </summary>
        private void ClearActiveLoadEntry()
        {
            //Отвязываем грид от модели
            WorkSheetDataGrid.DataSource = null;
            WorkSheetDataGrid.Refresh();

            //Сбрассываем статистику
            StatsDataGridView.Rows.Clear();
            StatsDataGridView.Columns.Clear();
            StatsDataGridView.Refresh();

            //Снимаем выделение и отключаем кнопку
            DBEntriesListBox.ClearSelected();
            ClearContentButton.Enabled = false;
            SecondHalfButton.Enabled = false;
        }

        #endregion
    }
}

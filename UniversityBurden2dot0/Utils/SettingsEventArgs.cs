﻿using System;

namespace UniversityBurden2dot0.Utils
{
    public class SettingsEventArgs : EventArgs
    {
        public bool DBPathChanged;

        public SettingsEventArgs(bool DBpathChanged)
        {
            this.DBPathChanged = DBpathChanged;
        }
    }
}

﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace UniversityBurden2dot0.Forms
{
    public partial class AboutForm : Form
    {
        public AboutForm()
        {
            InitializeComponent();
            LogoPictureBox.Image = Properties.Resources.lossfull_app_icon.ToBitmap();
            VersionLabel.Text = string.Format("v. {0}", Properties.Settings.Default.AppVersion);
            this.Width = AppNameLabel.Right + 50;
            this.Height = CreditsLabel.Bottom + 50;
            this.MinimumSize = this.MaximumSize = new Size(this.Width, this.Height);
        }
    }
}

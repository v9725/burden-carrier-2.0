﻿namespace UniversityBurden2dot0.Forms
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.CancelSettingsButton = new System.Windows.Forms.Button();
            this.ApplySettingsButton = new System.Windows.Forms.Button();
            this.HoursManualСheckBox = new System.Windows.Forms.CheckBox();
            this.DBPathTextBox = new System.Windows.Forms.TextBox();
            this.DBPathLabel = new System.Windows.Forms.Label();
            this.BrowseDBButton = new System.Windows.Forms.Button();
            this.AliasEditButton = new System.Windows.Forms.Button();
            this.DataSectionGroupBox = new System.Windows.Forms.GroupBox();
            this.secongHalfSectionGroupBox = new System.Windows.Forms.GroupBox();
            this.FooterPanel = new System.Windows.Forms.Panel();
            this.ContentPanel = new System.Windows.Forms.Panel();
            this.DataSectionGroupBox.SuspendLayout();
            this.secongHalfSectionGroupBox.SuspendLayout();
            this.FooterPanel.SuspendLayout();
            this.ContentPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // CancelSettingsButton
            // 
            this.CancelSettingsButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.CancelSettingsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CancelSettingsButton.Location = new System.Drawing.Point(80, 7);
            this.CancelSettingsButton.Name = "CancelSettingsButton";
            this.CancelSettingsButton.Size = new System.Drawing.Size(120, 41);
            this.CancelSettingsButton.TabIndex = 0;
            this.CancelSettingsButton.Text = "Скасувати";
            this.CancelSettingsButton.UseVisualStyleBackColor = true;
            this.CancelSettingsButton.Click += new System.EventHandler(this.CancelSettingsButton_Click);
            // 
            // ApplySettingsButton
            // 
            this.ApplySettingsButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ApplySettingsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ApplySettingsButton.Location = new System.Drawing.Point(229, 7);
            this.ApplySettingsButton.Name = "ApplySettingsButton";
            this.ApplySettingsButton.Size = new System.Drawing.Size(120, 41);
            this.ApplySettingsButton.TabIndex = 1;
            this.ApplySettingsButton.Text = "Застосувати";
            this.ApplySettingsButton.UseVisualStyleBackColor = true;
            this.ApplySettingsButton.Click += new System.EventHandler(this.ApplySettingsButton_Click);
            // 
            // HoursManualСheckBox
            // 
            this.HoursManualСheckBox.AutoSize = true;
            this.HoursManualСheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HoursManualСheckBox.Location = new System.Drawing.Point(8, 21);
            this.HoursManualСheckBox.Name = "HoursManualСheckBox";
            this.HoursManualСheckBox.Size = new System.Drawing.Size(327, 22);
            this.HoursManualСheckBox.TabIndex = 2;
            this.HoursManualСheckBox.Text = "Завжди запитувати ручний розподіл годин";
            this.HoursManualСheckBox.UseVisualStyleBackColor = true;
            this.HoursManualСheckBox.CheckStateChanged += new System.EventHandler(this.HoursManualСheckBox_CheckStateChanged);
            // 
            // DBPathTextBox
            // 
            this.DBPathTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DBPathTextBox.Location = new System.Drawing.Point(6, 41);
            this.DBPathTextBox.Name = "DBPathTextBox";
            this.DBPathTextBox.Size = new System.Drawing.Size(290, 24);
            this.DBPathTextBox.TabIndex = 3;
            this.DBPathTextBox.TextChanged += new System.EventHandler(this.DBPathTextBox_TextChanged);
            // 
            // DBPathLabel
            // 
            this.DBPathLabel.AutoSize = true;
            this.DBPathLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DBPathLabel.Location = new System.Drawing.Point(6, 20);
            this.DBPathLabel.Name = "DBPathLabel";
            this.DBPathLabel.Size = new System.Drawing.Size(156, 18);
            this.DBPathLabel.TabIndex = 4;
            this.DBPathLabel.Text = "Шлях до Бази Даних:";
            // 
            // BrowseDBButton
            // 
            this.BrowseDBButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BrowseDBButton.Location = new System.Drawing.Point(302, 40);
            this.BrowseDBButton.Name = "BrowseDBButton";
            this.BrowseDBButton.Size = new System.Drawing.Size(113, 25);
            this.BrowseDBButton.TabIndex = 5;
            this.BrowseDBButton.Text = "Вибрати...";
            this.BrowseDBButton.UseVisualStyleBackColor = true;
            this.BrowseDBButton.Click += new System.EventHandler(this.BrowseDBButton_Click);
            // 
            // AliasEditButton
            // 
            this.AliasEditButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AliasEditButton.Location = new System.Drawing.Point(9, 91);
            this.AliasEditButton.Name = "AliasEditButton";
            this.AliasEditButton.Size = new System.Drawing.Size(214, 31);
            this.AliasEditButton.TabIndex = 6;
            this.AliasEditButton.Text = "Налаштування Псевдонімів";
            this.AliasEditButton.UseVisualStyleBackColor = true;
            this.AliasEditButton.Click += new System.EventHandler(this.AliasEditButton_Click);
            // 
            // DataSectionGroupBox
            // 
            this.DataSectionGroupBox.Controls.Add(this.BrowseDBButton);
            this.DataSectionGroupBox.Controls.Add(this.DBPathLabel);
            this.DataSectionGroupBox.Controls.Add(this.AliasEditButton);
            this.DataSectionGroupBox.Controls.Add(this.DBPathTextBox);
            this.DataSectionGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DataSectionGroupBox.Location = new System.Drawing.Point(3, 3);
            this.DataSectionGroupBox.Name = "DataSectionGroupBox";
            this.DataSectionGroupBox.Size = new System.Drawing.Size(424, 128);
            this.DataSectionGroupBox.TabIndex = 7;
            this.DataSectionGroupBox.TabStop = false;
            this.DataSectionGroupBox.Text = "Дані";
            // 
            // secongHalfSectionGroupBox
            // 
            this.secongHalfSectionGroupBox.Controls.Add(this.HoursManualСheckBox);
            this.secongHalfSectionGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.secongHalfSectionGroupBox.Location = new System.Drawing.Point(3, 137);
            this.secongHalfSectionGroupBox.Name = "secongHalfSectionGroupBox";
            this.secongHalfSectionGroupBox.Size = new System.Drawing.Size(423, 51);
            this.secongHalfSectionGroupBox.TabIndex = 8;
            this.secongHalfSectionGroupBox.TabStop = false;
            this.secongHalfSectionGroupBox.Text = "Друга половина дня";
            // 
            // FooterPanel
            // 
            this.FooterPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FooterPanel.Controls.Add(this.CancelSettingsButton);
            this.FooterPanel.Controls.Add(this.ApplySettingsButton);
            this.FooterPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.FooterPanel.Location = new System.Drawing.Point(0, 193);
            this.FooterPanel.Name = "FooterPanel";
            this.FooterPanel.Size = new System.Drawing.Size(431, 57);
            this.FooterPanel.TabIndex = 9;
            // 
            // ContentPanel
            // 
            this.ContentPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ContentPanel.Controls.Add(this.secongHalfSectionGroupBox);
            this.ContentPanel.Controls.Add(this.DataSectionGroupBox);
            this.ContentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ContentPanel.Location = new System.Drawing.Point(0, 0);
            this.ContentPanel.Name = "ContentPanel";
            this.ContentPanel.Size = new System.Drawing.Size(431, 193);
            this.ContentPanel.TabIndex = 10;
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(431, 250);
            this.Controls.Add(this.ContentPanel);
            this.Controls.Add(this.FooterPanel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Налаштування";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SettingsForm_FormClosing);
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.DataSectionGroupBox.ResumeLayout(false);
            this.DataSectionGroupBox.PerformLayout();
            this.secongHalfSectionGroupBox.ResumeLayout(false);
            this.secongHalfSectionGroupBox.PerformLayout();
            this.FooterPanel.ResumeLayout(false);
            this.ContentPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button CancelSettingsButton;
        private System.Windows.Forms.Button ApplySettingsButton;
        private System.Windows.Forms.CheckBox HoursManualСheckBox;
        private System.Windows.Forms.TextBox DBPathTextBox;
        private System.Windows.Forms.Label DBPathLabel;
        private System.Windows.Forms.Button BrowseDBButton;
        private System.Windows.Forms.Button AliasEditButton;
        private System.Windows.Forms.GroupBox DataSectionGroupBox;
        private System.Windows.Forms.GroupBox secongHalfSectionGroupBox;
        private System.Windows.Forms.Panel FooterPanel;
        private System.Windows.Forms.Panel ContentPanel;
    }
}
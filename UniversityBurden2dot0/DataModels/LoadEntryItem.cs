﻿
namespace UniversityBurden2dot0.DataModels
{
    /// <summary>
    /// Класс, прeдставляющий программный аналог строки таблицы Load_Entries.
    /// </summary>
    public class LoadEntryItem : System.IComparable
    {
        #region Fields

        /// <summary>
        /// Семестр: осенний(1) или весенний(2).
        /// </summary>
        public int Semester { get; set; }

        /// <summary>
        /// Предмет.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Преподаватель.
        /// </summary>
        public string Teacher { get; set; }

        /// <summary>
        /// Вид учебной работы.
        /// </summary>
        public string StudyType { get; set; }

        /// <summary>
        /// Поток студентов.
        /// </summary>
        public string StudentFlow { get; set; }

        /// <summary>
        /// Количество часов.
        /// </summary>
        public int Hours { get; set; }

        #endregion

        #region Conturtor

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        /// <param name="semester">Семестр: осенний(1) или весенний(2).</param>
        /// <param name="subject">Предмет.</param>
        /// <param name="teacher">Преподаватель.</param>
        /// <param name="study_type">Вид учебной работы.</param>
        /// <param name="student_flow">Поток студентов.</param>
        /// <param name="hours">Количество часов.</param>
        public LoadEntryItem(
            int semester = -1,
            string subject = "",
            string teacher = "",
            string study_type = "",
            string student_flow = "",
            int hours = -1
        )
        {
            Semester = semester;
            Subject = subject;
            Teacher = teacher;
            StudyType = study_type;
            StudentFlow = student_flow;
            Hours = hours;
        }

        #endregion

        #region Methods

        public override string ToString()
        {
            return string.Format(
                "{0} Семестр|{1}|{2}|{3}|{4}|{5}", 
                this.Semester.ToString(), 
                this.Subject, 
                this.Teacher, 
                this.StudyType, 
                this.StudentFlow, 
                this.Hours.ToString()
                );
        }

        //Semester -> Subject (-> Teacher) -> Study_Type -> Student_Flow -> Hours
        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            if (obj is LoadEntryItem otherEntry)
            {
                int compResult = this.Semester.CompareTo(otherEntry.Semester);
                if (compResult == 0)
                {
                    compResult = this.Subject.CompareTo(otherEntry.Subject);
                    if(compResult == 0)
                    {
                        compResult = this.StudyType.CompareTo(otherEntry.StudyType);
                        if(compResult == 0)
                        {
                            compResult = this.StudentFlow.CompareTo(otherEntry.StudentFlow);
                            if(compResult == 0)
                            {
                                return this.Hours.CompareTo(otherEntry.Hours);
                            }
                        }
                    }
                }

                return compResult;
            }
            else
            {
                throw new System.ArgumentException("Object is not a Temperature");
            }
        }

        #endregion
    }
}

﻿namespace UniversityBurden2dot0.Forms
{
    partial class SecondHalfMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.HeaderLabel1 = new System.Windows.Forms.Label();
            this.HoursTotalTextBox = new System.Windows.Forms.TextBox();
            this.HeaderPanel = new System.Windows.Forms.Panel();
            this.HeaderLabel4 = new System.Windows.Forms.Label();
            this.HoursSecondHalfTotalTextBox = new System.Windows.Forms.TextBox();
            this.HeaderLabel3 = new System.Windows.Forms.Label();
            this.HoursSecondHalfLeftTextBox = new System.Windows.Forms.TextBox();
            this.HeaderLabel2 = new System.Windows.Forms.Label();
            this.HoursFirstHalfTextBox = new System.Windows.Forms.TextBox();
            this.MainGridView = new System.Windows.Forms.DataGridView();
            this.GridPanel = new System.Windows.Forms.Panel();
            this.FooterPanel = new System.Windows.Forms.Panel();
            this.ExportToDocButton = new System.Windows.Forms.Button();
            this.AutoSecondHalfButton = new System.Windows.Forms.Button();
            this.FormCancelButton = new System.Windows.Forms.Button();
            this.WorkTypeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AllocatedHoursColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MinHoursColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecommendedHoursColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaxHoursColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CommentColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HeaderPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainGridView)).BeginInit();
            this.GridPanel.SuspendLayout();
            this.FooterPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // HeaderLabel1
            // 
            this.HeaderLabel1.AutoSize = true;
            this.HeaderLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HeaderLabel1.Location = new System.Drawing.Point(169, 5);
            this.HeaderLabel1.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.HeaderLabel1.Name = "HeaderLabel1";
            this.HeaderLabel1.Size = new System.Drawing.Size(330, 24);
            this.HeaderLabel1.TabIndex = 4;
            this.HeaderLabel1.Text = "усього годин навантаження. З них:";
            // 
            // HoursTotalTextBox
            // 
            this.HoursTotalTextBox.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.HoursTotalTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HoursTotalTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.HoursTotalTextBox.Location = new System.Drawing.Point(119, 3);
            this.HoursTotalTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.HoursTotalTextBox.MaxLength = 4;
            this.HoursTotalTextBox.Name = "HoursTotalTextBox";
            this.HoursTotalTextBox.Size = new System.Drawing.Size(49, 28);
            this.HoursTotalTextBox.TabIndex = 4;
            this.HoursTotalTextBox.Text = "1548";
            this.HoursTotalTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.HoursTotalTextBox.TextChanged += new System.EventHandler(this.HoursTotalTextBox_TextChanged);
            this.HoursTotalTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HoursTotalTextBox_KeyPress);
            // 
            // HeaderPanel
            // 
            this.HeaderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.HeaderPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.HeaderPanel.Controls.Add(this.HeaderLabel4);
            this.HeaderPanel.Controls.Add(this.HoursSecondHalfTotalTextBox);
            this.HeaderPanel.Controls.Add(this.HeaderLabel3);
            this.HeaderPanel.Controls.Add(this.HoursSecondHalfLeftTextBox);
            this.HeaderPanel.Controls.Add(this.HeaderLabel2);
            this.HeaderPanel.Controls.Add(this.HoursFirstHalfTextBox);
            this.HeaderPanel.Controls.Add(this.HoursTotalTextBox);
            this.HeaderPanel.Controls.Add(this.HeaderLabel1);
            this.HeaderPanel.Location = new System.Drawing.Point(9, 9);
            this.HeaderPanel.Margin = new System.Windows.Forms.Padding(0);
            this.HeaderPanel.Name = "HeaderPanel";
            this.HeaderPanel.Size = new System.Drawing.Size(1278, 37);
            this.HeaderPanel.TabIndex = 3;
            // 
            // HeaderLabel4
            // 
            this.HeaderLabel4.AutoSize = true;
            this.HeaderLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HeaderLabel4.Location = new System.Drawing.Point(956, 5);
            this.HeaderLabel4.Margin = new System.Windows.Forms.Padding(2, 0, 1, 0);
            this.HeaderLabel4.Name = "HeaderLabel4";
            this.HeaderLabel4.Size = new System.Drawing.Size(201, 24);
            this.HeaderLabel4.TabIndex = 10;
            this.HeaderLabel4.Text = "другої половини дня.";
            // 
            // HoursSecondHalfTotalTextBox
            // 
            this.HoursSecondHalfTotalTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.HoursSecondHalfTotalTextBox.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.HoursSecondHalfTotalTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HoursSecondHalfTotalTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.HoursSecondHalfTotalTextBox.Location = new System.Drawing.Point(905, 3);
            this.HoursSecondHalfTotalTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.HoursSecondHalfTotalTextBox.Name = "HoursSecondHalfTotalTextBox";
            this.HoursSecondHalfTotalTextBox.ReadOnly = true;
            this.HoursSecondHalfTotalTextBox.Size = new System.Drawing.Size(49, 28);
            this.HoursSecondHalfTotalTextBox.TabIndex = 9;
            this.HoursSecondHalfTotalTextBox.Text = "0000";
            this.HoursSecondHalfTotalTextBox.TextChanged += new System.EventHandler(this.HoursSecondHalfTotalTextBox_TextChanged);
            // 
            // HeaderLabel3
            // 
            this.HeaderLabel3.AutoSize = true;
            this.HeaderLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HeaderLabel3.Location = new System.Drawing.Point(884, 5);
            this.HeaderLabel3.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.HeaderLabel3.Name = "HeaderLabel3";
            this.HeaderLabel3.Size = new System.Drawing.Size(18, 25);
            this.HeaderLabel3.TabIndex = 8;
            this.HeaderLabel3.Text = "/";
            // 
            // HoursSecondHalfLeftTextBox
            // 
            this.HoursSecondHalfLeftTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.HoursSecondHalfLeftTextBox.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.HoursSecondHalfLeftTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HoursSecondHalfLeftTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.HoursSecondHalfLeftTextBox.Location = new System.Drawing.Point(833, 3);
            this.HoursSecondHalfLeftTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.HoursSecondHalfLeftTextBox.Name = "HoursSecondHalfLeftTextBox";
            this.HoursSecondHalfLeftTextBox.ReadOnly = true;
            this.HoursSecondHalfLeftTextBox.Size = new System.Drawing.Size(49, 28);
            this.HoursSecondHalfLeftTextBox.TabIndex = 7;
            this.HoursSecondHalfLeftTextBox.Text = "0000";
            this.HoursSecondHalfLeftTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // HeaderLabel2
            // 
            this.HeaderLabel2.AutoSize = true;
            this.HeaderLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HeaderLabel2.Location = new System.Drawing.Point(554, 5);
            this.HeaderLabel2.Margin = new System.Windows.Forms.Padding(2, 0, 1, 0);
            this.HeaderLabel2.Name = "HeaderLabel2";
            this.HeaderLabel2.Size = new System.Drawing.Size(275, 24);
            this.HeaderLabel2.TabIndex = 6;
            this.HeaderLabel2.Text = "навачального навантаження,";
            // 
            // HoursFirstHalfTextBox
            // 
            this.HoursFirstHalfTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.HoursFirstHalfTextBox.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.HoursFirstHalfTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HoursFirstHalfTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.HoursFirstHalfTextBox.Location = new System.Drawing.Point(503, 3);
            this.HoursFirstHalfTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.HoursFirstHalfTextBox.Name = "HoursFirstHalfTextBox";
            this.HoursFirstHalfTextBox.ReadOnly = true;
            this.HoursFirstHalfTextBox.Size = new System.Drawing.Size(49, 28);
            this.HoursFirstHalfTextBox.TabIndex = 5;
            this.HoursFirstHalfTextBox.Text = "0000";
            this.HoursFirstHalfTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MainGridView
            // 
            this.MainGridView.AllowUserToAddRows = false;
            this.MainGridView.AllowUserToDeleteRows = false;
            this.MainGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MainGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.WorkTypeColumn,
            this.AllocatedHoursColumn,
            this.MinHoursColumn,
            this.RecommendedHoursColumn,
            this.MaxHoursColumn,
            this.CommentColumn});
            this.MainGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainGridView.Location = new System.Drawing.Point(0, 0);
            this.MainGridView.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.MainGridView.Name = "MainGridView";
            this.MainGridView.RowHeadersVisible = false;
            this.MainGridView.RowTemplate.Height = 24;
            this.MainGridView.Size = new System.Drawing.Size(1276, 707);
            this.MainGridView.TabIndex = 4;
            this.MainGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.MainGridView_CellEndEdit);
            // 
            // GridPanel
            // 
            this.GridPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GridPanel.Controls.Add(this.MainGridView);
            this.GridPanel.Location = new System.Drawing.Point(9, 49);
            this.GridPanel.Name = "GridPanel";
            this.GridPanel.Size = new System.Drawing.Size(1278, 709);
            this.GridPanel.TabIndex = 5;
            // 
            // FooterPanel
            // 
            this.FooterPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FooterPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FooterPanel.Controls.Add(this.ExportToDocButton);
            this.FooterPanel.Controls.Add(this.AutoSecondHalfButton);
            this.FooterPanel.Controls.Add(this.FormCancelButton);
            this.FooterPanel.Location = new System.Drawing.Point(9, 764);
            this.FooterPanel.Name = "FooterPanel";
            this.FooterPanel.Size = new System.Drawing.Size(1278, 57);
            this.FooterPanel.TabIndex = 6;
            // 
            // ExportToDocButton
            // 
            this.ExportToDocButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ExportToDocButton.Location = new System.Drawing.Point(735, 4);
            this.ExportToDocButton.Name = "ExportToDocButton";
            this.ExportToDocButton.Size = new System.Drawing.Size(182, 45);
            this.ExportToDocButton.TabIndex = 2;
            this.ExportToDocButton.Text = "Експортувати до інд. плану";
            this.ExportToDocButton.UseVisualStyleBackColor = true;
            this.ExportToDocButton.Click += new System.EventHandler(this.ExportToDocButton_Click);
            // 
            // AutoSecondHalfButton
            // 
            this.AutoSecondHalfButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.AutoSecondHalfButton.Location = new System.Drawing.Point(547, 4);
            this.AutoSecondHalfButton.Name = "AutoSecondHalfButton";
            this.AutoSecondHalfButton.Size = new System.Drawing.Size(182, 45);
            this.AutoSecondHalfButton.TabIndex = 0;
            this.AutoSecondHalfButton.Text = "Сформувати \r\nавтоматично";
            this.AutoSecondHalfButton.UseVisualStyleBackColor = true;
            this.AutoSecondHalfButton.Click += new System.EventHandler(this.AutoSecondHalfButton_Click);
            // 
            // FormCancelButton
            // 
            this.FormCancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.FormCancelButton.Location = new System.Drawing.Point(359, 4);
            this.FormCancelButton.Name = "FormCancelButton";
            this.FormCancelButton.Size = new System.Drawing.Size(182, 45);
            this.FormCancelButton.TabIndex = 3;
            this.FormCancelButton.Text = "Скасувати";
            this.FormCancelButton.UseVisualStyleBackColor = true;
            this.FormCancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // WorkTypeColumn
            // 
            this.WorkTypeColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.WorkTypeColumn.HeaderText = "Тип Робот";
            this.WorkTypeColumn.Name = "WorkTypeColumn";
            this.WorkTypeColumn.ReadOnly = true;
            this.WorkTypeColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.WorkTypeColumn.ToolTipText = "Перелік доступних типів робіт для другої половини дня";
            this.WorkTypeColumn.Width = 83;
            // 
            // AllocatedHoursColumn
            // 
            this.AllocatedHoursColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.AllocatedHoursColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.AllocatedHoursColumn.HeaderText = "Години";
            this.AllocatedHoursColumn.MaxInputLength = 3;
            this.AllocatedHoursColumn.Name = "AllocatedHoursColumn";
            this.AllocatedHoursColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AllocatedHoursColumn.ToolTipText = "Кількість годин, виділенена для типу роботи";
            this.AllocatedHoursColumn.Width = 62;
            // 
            // MinHoursColumn
            // 
            this.MinHoursColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.MinHoursColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.MinHoursColumn.HeaderText = "Мін.";
            this.MinHoursColumn.Name = "MinHoursColumn";
            this.MinHoursColumn.ReadOnly = true;
            this.MinHoursColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MinHoursColumn.ToolTipText = "Мінімально допустима кількість годин";
            this.MinHoursColumn.Width = 40;
            // 
            // RecommendedHoursColumn
            // 
            this.RecommendedHoursColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.RecommendedHoursColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.RecommendedHoursColumn.HeaderText = "Рекоменд.";
            this.RecommendedHoursColumn.Name = "RecommendedHoursColumn";
            this.RecommendedHoursColumn.ReadOnly = true;
            this.RecommendedHoursColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.RecommendedHoursColumn.ToolTipText = "Рекомендована кількість годин";
            this.RecommendedHoursColumn.Width = 83;
            // 
            // MaxHoursColumn
            // 
            this.MaxHoursColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.MaxHoursColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.MaxHoursColumn.HeaderText = "Макс.";
            this.MaxHoursColumn.Name = "MaxHoursColumn";
            this.MaxHoursColumn.ReadOnly = true;
            this.MaxHoursColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MaxHoursColumn.ToolTipText = "Максимально допустима кількість годин";
            this.MaxHoursColumn.Width = 51;
            // 
            // CommentColumn
            // 
            this.CommentColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CommentColumn.HeaderText = "Коментар";
            this.CommentColumn.Name = "CommentColumn";
            this.CommentColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CommentColumn.ToolTipText = "Коментар до типу роботы";
            // 
            // SecondHalfMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1296, 833);
            this.Controls.Add(this.FooterPanel);
            this.Controls.Add(this.GridPanel);
            this.Controls.Add(this.HeaderPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SecondHalfMenuForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Меню Другої половини дня";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SecondHalfMenuForm_FormClosing);
            this.Load += new System.EventHandler(this.SecondHalfMenuForm_Load);
            this.HeaderPanel.ResumeLayout(false);
            this.HeaderPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainGridView)).EndInit();
            this.GridPanel.ResumeLayout(false);
            this.FooterPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label HeaderLabel1;
        private System.Windows.Forms.TextBox HoursTotalTextBox;
        private System.Windows.Forms.Panel HeaderPanel;
        private System.Windows.Forms.DataGridView MainGridView;
        private System.Windows.Forms.Panel GridPanel;
        private System.Windows.Forms.Panel FooterPanel;
        private System.Windows.Forms.Button ExportToDocButton;
        private System.Windows.Forms.Button AutoSecondHalfButton;
        private System.Windows.Forms.Button FormCancelButton;
        private System.Windows.Forms.TextBox HoursFirstHalfTextBox;
        private System.Windows.Forms.TextBox HoursSecondHalfLeftTextBox;
        private System.Windows.Forms.Label HeaderLabel2;
        private System.Windows.Forms.Label HeaderLabel3;
        private System.Windows.Forms.TextBox HoursSecondHalfTotalTextBox;
        private System.Windows.Forms.Label HeaderLabel4;
        private System.Windows.Forms.DataGridViewTextBoxColumn WorkTypeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn AllocatedHoursColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn MinHoursColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecommendedHoursColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaxHoursColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CommentColumn;
    }
}